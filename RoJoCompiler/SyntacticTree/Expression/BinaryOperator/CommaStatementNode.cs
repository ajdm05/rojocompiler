﻿using RoJoCompiler.SyntacticTree.Statement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Expression.BinaryOperator
{
    public class CommaStatementNode : StatementNode
    {
        public string ValueO { get; set; }

        public StatementNode LeftOperand { get; set; }

        public StatementNode RightOperand { get; set; }

        public override void ValidateSemantic()
        {
            throw new NotImplementedException();
        }

        public override void Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
