﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Interpretation
{
    public class FloatValue : Value
    {
        public float MyValue { get; set; }

        public override Value Clone()
        {
            return new FloatValue { MyValue = this.MyValue };
        }
    }
}
