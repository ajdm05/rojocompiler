﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoJoCompiler.Lexicon
{
    public class LexiconException : Exception
    {
        public LexiconException(){}

        public LexiconException(string message) : base(message) { }
    }
}
