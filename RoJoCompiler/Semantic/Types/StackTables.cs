﻿using RoJoCompiler.Interpretation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Semantic.Types
{
    public class StackTables
    {
        private static StackTables _stackInstance { get; set; }

        public Stack<SymbolsTable> Stack { get; set; }

        private StackTables()
        {
            Stack = new Stack<SymbolsTable>();
            Stack.Push(SymbolsTable.Instance);
        }

        public static StackTables StackInstance 
        {
            get
            {
                if (_stackInstance == null)
                    _stackInstance = new StackTables();
                return _stackInstance;
            }
        }

        public bool ExistVariableInStack(string name)
        {
            foreach(var stack in Stack)
            {
                if (stack.ExistVariable(name))
                    return true;
            }
            return false;
        }

        public DataType GetVariable(string name)
        {
            foreach (var stack in Stack)
            {
                if (stack.ExistVariable(name))
                    return stack.GetVariable(name);
            }
            return null;
        } 

        public void SetStackVariableValue(string name, Value myValue)
        {
            foreach(var stack in Stack)
            {
                if (stack.ExistVariable(name))
                    stack.SetVariableValue(name, myValue);
            }
        }

        public Value GetStackVariableValue(string name)
        {
            foreach (var stack in Stack)
            {
                if (stack.ExistVariable(name))
                    return stack.GetVariableValue(name);
            }
            return null;
        }
    }
}
