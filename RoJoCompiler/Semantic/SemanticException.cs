﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Semantic
{
    public class SemanticException : Exception
    {
        public SemanticException(){}

        public SemanticException(string message) : base(message) { }
    }
}
