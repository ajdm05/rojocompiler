﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class SwitchNode : StatementNode
    {

        public ExpressionNode Condition { get; set; }

        public List<CaseNode> CaseList { get; set; }

        public DefaultNode Default { get; set; }

        public override void ValidateSemantic()
        {
            throw new NotImplementedException();
        }

        public override void Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
