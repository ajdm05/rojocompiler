﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Expression.BinaryOperator
{
    public class MultiplicationAssignmentNode : BinaryOperatorNode
    {
        public override DataType EvaluateSemantic()
        {
            var leftType = LeftOperand.EvaluateSemantic();
            var rightType = RightOperand.EvaluateSemantic();
            if (leftType.GetType() == rightType.GetType())
            {
                if (leftType is IntType || leftType is BoolType)
                    return leftType;
            }
            throw new SemanticException("Could not multiply between" + leftType + " and " + rightType);
        }

        public override Value Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
