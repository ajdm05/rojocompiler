﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class WhileNode : StatementNode
    {
        public ExpressionNode Condition { get; set; }

        public List<StatementNode> LoopBlock { get; set; }

        public override void ValidateSemantic()
        {
            throw new NotImplementedException();
        }

        public override void Interpret()
        {
            
        }
    }
}
