﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoJoCompiler.SyntacticTree.Expression.Identifier;
using RoJoCompiler.Semantic.Types;
using RoJoCompiler.Semantic;
using RoJoCompiler.SyntacticTree.Expression.BinaryOperator;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class AssignmentStatementNode : StatementNode
    {
        public IdNode LeftOperand { get; set; }

        public ExpressionNode RightOperand { get; set; }

        public IdArrayNode Array { get; set; }

        public List<ExpressionNode> ArrayInitializer { get; set; }

        public override void ValidateSemantic()
        {
            if (RightOperand == null && LeftOperand == null)
                this.Array.ValidateSemantic();
            else
            {
                if(RightOperand is AssignmentNode || RightOperand is AdditionAssignmentNode || RightOperand is SubtractionAssignmentNode || RightOperand is MultiplicationAssignmentNode ||
                    RightOperand is DivisionAssignmentNode || RightOperand is ModAssignmentNode || RightOperand is BinaryAndAssignmentNode || RightOperand is BinaryOrAssignmentNode ||
                    RightOperand is BinaryShiftLeftAssignmentNode || RightOperand is BinaryShiftRightAssignmentNode || RightOperand is BinaryXorAssignmentNode)
                    RightOperand.EvaluateSemantic();
                else
                {
                    if(string.IsNullOrEmpty(LeftOperand.DataType))
                    {
                        if (!StackTables.StackInstance.ExistVariableInStack(LeftOperand.Name))
                            throw new SemanticException("Variable " + LeftOperand.Name + " in row " + CurrentToken.Row + " and column " + CurrentToken.Column + " does not exist.");
                    } 
                    else
                    {
                        if (!StackTables.StackInstance.ExistVariableInStack(LeftOperand.Name))
                            StackTables.StackInstance.Stack.Peek().DeclareVariable(LeftOperand.Name, this.GetType(LeftOperand.DataType));
                        else
                            throw new SemanticException("Variable " + LeftOperand.Name + " in row " + CurrentToken.Row + " and column " + CurrentToken.Column + " already exists.");
                    }

                    var rightType = RightOperand.EvaluateSemantic();
                    var leftType = StackTables.StackInstance.GetVariable(LeftOperand.Name);
                    if (rightType.GetType() != leftType.GetType())
                        throw new SemanticException("Could not assign " + rightType + " to " + leftType + " in row " + CurrentToken.Row + " column " + CurrentToken.Column + ".");
                }
            }
        }

        public override void Interpret()
        {
            if (RightOperand == null && LeftOperand == null)
                this.Array.Interpret();
            else
            {
                if (RightOperand is AssignmentNode || RightOperand is AdditionAssignmentNode || RightOperand is SubtractionAssignmentNode || RightOperand is MultiplicationAssignmentNode ||
                    RightOperand is DivisionAssignmentNode || RightOperand is ModAssignmentNode || RightOperand is BinaryAndAssignmentNode || RightOperand is BinaryOrAssignmentNode ||
                    RightOperand is BinaryShiftLeftAssignmentNode || RightOperand is BinaryShiftRightAssignmentNode || RightOperand is BinaryXorAssignmentNode)
                    RightOperand.Interpret();
                else
                {
                    if(LeftOperand != null)
                    {
                        var varName = ((IdNode)LeftOperand).Name;
                        var rightValue = RightOperand.Interpret();
                        var varValue = StackTables.StackInstance.GetStackVariableValue(varName);
                        if (rightValue.GetType() == varValue.GetType())
                            StackTables.StackInstance.SetStackVariableValue(varName, rightValue);
                    }
                }
            }
        
        
        
        
        
        
        
        
        
        
        }

        private DataType GetType(string type)
        {
            switch (type)
            {
                case "bool":
                    return new BoolType();
                case "char":
                    return new CharType();
                case "date":
                    return new DateType();
                case "enum":
                    return new EnumType();
                case "float":
                    return new FloatType();
                case "int":
                    return new IntType();
                case "struct":
                    return new StructType();
                case "string":
                    return new StringType();
                case "void":
                    return new VoidType();
                default:
                    return null;
            }
        }
    }
}
