﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoJoCompiler.SyntacticTree.Expression.Identifier;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class StructNode : StatementNode
    {
        public IdNode Id { get; set; }

        public List<StatementNode> Members { get; set; }

        public List<StatementNode> Varibles { get; set; }

        public override void ValidateSemantic()
        {
            throw new NotImplementedException();
        }

        public override void Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
