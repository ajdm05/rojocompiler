﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Expression.Literal
{
    public class FloatLiteralNode : ExpressionNode
    {
        public float Value { get; set; }

        public override DataType EvaluateSemantic()
        {
            return new FloatType();
        }

        public override Value Interpret()
        {
            return new FloatValue { MyValue = this.Value };
        }
    }
}
