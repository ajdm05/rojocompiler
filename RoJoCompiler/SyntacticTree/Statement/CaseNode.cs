﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class CaseNode : StatementNode
    {
        public ExpressionNode Condition { get; set; }

        public List<StatementNode> StatementsBlock { get; set; }

        public BreakNode Break { get; set; }

        public override void ValidateSemantic()
        {
            throw new NotImplementedException();
        }

        public override void Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
