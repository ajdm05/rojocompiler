﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoJoCompiler.SyntacticTree.Expression.Identifier;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class ForEachNode : ForGeneralNode
    {
        public string DataType { get; set; }

        public IdNode Variable { get; set; }

        public IdNode ListLoop { get; set; }
    }
}
