﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoJoCompiler.SyntacticTree.Expression.Identifier;
using RoJoCompiler.Semantic.Types;
using RoJoCompiler.Semantic;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class GeneralDeclarationNode : StatementNode
    {
        public string DataType { get; set; }

        public List<ExpressionNode> Pointers { get; set; }

        public IdNode Id { get; set; }

        public override void ValidateSemantic()
        {
            if (StackTables.StackInstance.ExistVariableInStack(Id.Name))
                throw new SemanticException("Variable " + Id.Name + " in row " + CurrentToken.Row + " and column " + CurrentToken.Column + " already exists.");
            else
            {
                if(Pointers != null && Pointers.Count > 0)
                {
                    var pointer = new PointerType { MyType = GetType(DataType) };
                    StackTables.StackInstance.Stack.Peek().DeclareVariable(Id.Name, pointer);
                } 
                else 
                    StackTables.StackInstance.Stack.Peek().DeclareVariable(Id.Name, GetType(DataType));
            }
        }

        public override void Interpret()
        {
            
        }

        private DataType GetType(string type)
        {
            switch (type)
            {
                case "bool":
                    return new BoolType();
                case "char":
                    return new CharType();
                case "date":
                    return new DateType();
                case "enum":
                    return new EnumType();
                case "float":
                    return new FloatType();
                case "int":
                    return new IntType();
                case "struct":
                    return new StructType();
                case "string":
                    return new StringType();
                case "void":
                    return new VoidType();
                default:
                    return null;
            }
        }
    }
}
