﻿using RoJoCompiler.Semantic.Types;
using RoJoCompiler.SyntacticTree.Expression.BinaryOperator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoJoCompiler.SyntacticTree.Expression.Identifier;
using RoJoCompiler.Semantic;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class ConstNode : StatementNode
    {
        public IdNode Id { get; set; }

        public ExpressionNode Initialitation { get; set; }

        public override void ValidateSemantic()
        {
            var initType = Initialitation.EvaluateSemantic();
            if (!SymbolsTable.Instance.ExistVariable(Id.Name))
                SymbolsTable.Instance.DeclareVariable(Id.Name, initType);

            if (Initialitation == null)
                throw new SemanticException("The variable " + Id.Name + " must be initialized.");
        }

        public override void Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
