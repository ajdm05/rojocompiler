﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Interpretation
{
    public class IntValue : Value
    {
        public int MyValue { get; set; }

        public override Value Clone()
        {
            return new IntValue { MyValue = this.MyValue };
        }
    }
}
