﻿using RoJoCompiler.Semantic;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Accessor
{
    public class ArrayAccessorNode : AccessorNode
    {
        public ExpressionNode MySize { get; set; }

        public override void ValidateSemantic()
        {
            var mySize = MySize.EvaluateSemantic();
            if (!(mySize is IntType))
                throw new SemanticException("The array size in row " + CurrentToken.Row + " and column " + CurrentToken.Column + " mus be a number type.");
        }
    }
}
