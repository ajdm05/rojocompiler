﻿using RoJoCompiler.Interpretation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Semantic.Types
{
    public class DateType : DataType
    {
        public override Value GetDefaultValue()
        {
            return new DateValue { MyValue = new DateTime() };
        }

        public override string ToString()
        {
            return "date";
        }
    }
}
