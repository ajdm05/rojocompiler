﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic;
using RoJoCompiler.Semantic.Types;
using RoJoCompiler.SyntacticTree.Expression.Identifier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Expression.BinaryOperator
{
    public class AssignmentNode : BinaryOperatorNode
    {
        public override DataType EvaluateSemantic()
        {
            var rightType = RightOperand.EvaluateSemantic();
            var idNode = ((IdNode)LeftOperand);
            if (string.IsNullOrEmpty(idNode.DataType))
            {
                var leftType = idNode.EvaluateSemantic();
                if (leftType.GetType() != RightOperand.GetType())
                    throw new SemanticException("Could not assign " + rightType + " to " + leftType + " in row " + CurrentToken.Row + " column " + CurrentToken.Column + ".");
                return leftType;
            }
            else if (!StackTables.StackInstance.ExistVariableInStack(idNode.Name))
            {
                var idType = this.GetType(idNode.DataType);
                StackTables.StackInstance.Stack.Peek().DeclareVariable(idNode.Name, idType);
                return idType;
            }
            else
            {
                throw new SemanticException("Variable " + idNode.Name + " in row " + CurrentToken.Row + " and column " + CurrentToken.Column + " already exists.");
            }
        }

        public override Value Interpret()
        {
            var rightValue = RightOperand.Interpret();
            if(LeftOperand is IdNode)
            {
                var varName = ((IdNode)LeftOperand).Name;
                var valueType = StackTables.StackInstance.GetStackVariableValue(varName);
                if (valueType.GetType() == rightValue.GetType())
                    StackTables.StackInstance.SetStackVariableValue(varName, rightValue);
            }
            
            return rightValue;
        }

        private DataType GetType(string type)
        {
            switch (type)
            {
                case "bool":
                    return new BoolType();
                case "char":
                    return new CharType();
                case "date":
                    return new DateType();
                case "enum":
                    return new EnumType();
                case "float":
                    return new FloatType();
                case "int":
                    return new IntType();
                case "struct":
                    return new StructType();
                case "string":
                    return new StringType();
                case "void":
                    return new VoidType();
                default:
                    return null;
            }
        }
    }
}
