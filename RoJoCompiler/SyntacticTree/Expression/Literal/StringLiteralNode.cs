﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Expression.Literal
{
    public class StringLiteralNode : ExpressionNode
    {
        public string Value { get; set; }

        public override DataType EvaluateSemantic()
        {
            return new StringType();
        }

        public override Value Interpret()
        {
            return new StringValue { MyValue = this.Value };
        }
    }
}
