﻿using RoJoCompiler.Semantic;
using RoJoCompiler.Semantic.Types;
using RoJoCompiler.SyntacticTree.Accessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoJoCompiler.SyntacticTree.Statement;
using RoJoCompiler.SyntacticTree.Expression.BinaryOperator;
using RoJoCompiler.Interpretation;

namespace RoJoCompiler.SyntacticTree.Expression.Identifier
{
    public class IdNode : ExpressionNode
    {
        public string DataType { get; set; }

        public string Name { get; set; }

        public List<ExpressionNode> Pointers { get; set; }

        public List<AccessorNode> Accessors { get; set; }

        public override DataType EvaluateSemantic()
        {
            if (!SymbolsTable.Instance.ExistVariable(Name))
                throw new SemanticException("Variable " + Name + " in row " + CurrentToken.Row + " and column " + CurrentToken.Column + " does not exist.");
            return SymbolsTable.Instance.GetVariable(Name);
        }

        public override Value Interpret()
        {
            if (StackTables.StackInstance.ExistVariableInStack(Name))
                return StackTables.StackInstance.GetStackVariableValue(Name);
            return null;
        }
    }
}
