﻿using RoJoCompiler.Interpretation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Semantic.Types
{
    public class OctalLiteral : DataType
    {
        public override Value GetDefaultValue()
        {
            return new IntValue();
        }
    }
}
