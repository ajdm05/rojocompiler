﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Expression.Literal
{
    public class CharLiteralNode : ExpressionNode
    {
        public char Value { get; set; }

        public override DataType EvaluateSemantic()
        {
            return new CharType();
        }

        public override Value Interpret()
        {
            return new CharValue { MyValue = this.Value };
        }
    }
}
