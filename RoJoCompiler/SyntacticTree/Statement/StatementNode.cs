﻿using RoJoCompiler.Lexicon;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public abstract class StatementNode
    {
        public Token CurrentToken { get; set; }

        public abstract void ValidateSemantic();

        public abstract void Interpret();
    }
}
