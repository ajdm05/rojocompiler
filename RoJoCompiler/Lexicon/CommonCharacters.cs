﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoJoCompiler.Lexicon
{
    public class CommonCharacters
    {
        public static readonly char Enter = '\n';

        public static readonly char Not = '!';

        public static readonly char BitwiseAnd = '&';

        public static readonly char BitwiseOr = '|';
            
        public static readonly char LessThan = '<';

        public static readonly char GreaterThan = '>';

        public static readonly char Mod = '%';

        public static readonly char EndOfSentence = ';';

        public static readonly char LowDash = '_';

        public static readonly char MiddleDash = '-';

        public static readonly char EndOfFile = '\0';

        public static readonly char Apostrophe = '\'';

        public static readonly char BackSlash = '\\';

        public static readonly char Assignment = '=';

        public static readonly char Point = '.';

        public static readonly char Slash = '/';

        public static readonly char Asterisk = '*';

        public static readonly char DittoMark = '"';

        public static readonly char Sharp = '#';

        public static readonly char Zero = '0';

        public static readonly char One = '1';

        public static readonly char Two = '2';

        public static readonly char Three = '3';

        public static readonly char Four = '4';

        public static readonly char Five = '5';

        public static readonly char Six = '6';

        public static readonly char Seven = '7';

        public static readonly char U = 'U';

        public static readonly char X = 'x';

        public static readonly char A = 'A';

        public static readonly char B = 'B';

        public static readonly char C = 'C';

        public static readonly char D = 'D';

        public static readonly char E = 'E';

        public static readonly char F = 'F';

        public static readonly char LowerI = 'i';

        public static readonly char LowerN = 'n';

        public static readonly char LowerC = 'c';

        public static readonly char LowerL = 'l';
        
        public static readonly char LowerU = 'u';

        public static readonly char LowerD = 'd';

        public static readonly char LowerE = 'e';
    }
}
