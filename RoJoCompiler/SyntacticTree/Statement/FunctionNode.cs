﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoJoCompiler.SyntacticTree.Expression.Identifier;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class FunctionNode : StatementNode
    {
        public string DataType { get; set; }

        public IdNode Id { get; set; }

        public List<StatementNode> Parameters { get; set; }

        public List<StatementNode> StatementsBlock { get; set; }

        public override void ValidateSemantic()
        {
            throw new NotImplementedException();
        }

        public override void Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
