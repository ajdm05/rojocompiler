﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Lexicon;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoJoCompiler.SyntacticTree
{
    public abstract class ExpressionNode
    {
        public Token CurrentToken { get; set; }

        public abstract DataType EvaluateSemantic();

        public abstract Value Interpret();
    }
}
