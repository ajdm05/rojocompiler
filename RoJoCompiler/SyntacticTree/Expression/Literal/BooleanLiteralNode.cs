﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Expression.Literal
{
    public class BooleanLiteralNode : ExpressionNode
    {
        public bool Value { get; set; }

        public override DataType EvaluateSemantic()
        {
            return new BoolType();
        }

        public override Value Interpret()
        {
            return new BoolValue { MyValue = this.Value };
        }
    }
}
