﻿using RoJoCompiler.Interpretation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Semantic.Types
{
    public class FloatType : DataType
    {
        public override Value GetDefaultValue()
        {
            return new FloatValue { MyValue = 0 };
        }

        public override string ToString()
        {
            return "float";
        }
    }
}
