﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoJoCompiler.SyntacticTree.Expression.BinaryOperator
{
    public abstract class BinaryOperatorNode : ExpressionNode
    {
        public string ValueO { get; set; }

        public ExpressionNode LeftOperand { get; set; }

        public ExpressionNode RightOperand { get; set; }
    }
}
