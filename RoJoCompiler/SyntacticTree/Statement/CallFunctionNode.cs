﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoJoCompiler.SyntacticTree.Expression.Identifier;
using RoJoCompiler.Semantic.Types;
using RoJoCompiler.Interpretation;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class CallFunctionNode : ExpressionNode
    {

        public IdNode Id { get; set; }

        public List<ExpressionNode> Parameters { get; set; }

        public override DataType EvaluateSemantic()
        {
            return new IntType();
        }

        public override Value Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
