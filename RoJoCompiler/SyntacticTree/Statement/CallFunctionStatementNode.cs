﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoJoCompiler.SyntacticTree.Expression.Identifier;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class CallFunctionStatementNode : StatementNode
    {
        public IdNode Id { get; set; }

        public List<ExpressionNode> Parameters { get; set; }

        public override void ValidateSemantic()
        {
            throw new NotImplementedException();
        }

        public override void Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
