﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Interpretation
{
    public class CharValue : Value
    {
        public char MyValue { get; set; }

        public override Value Clone()
        {
            return new CharValue { MyValue = this.MyValue };
        }
    }
}
