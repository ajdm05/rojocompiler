﻿using RoJoCompiler.Interpretation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Semantic.Types
{
    public class ArrayType : DataType
    {
        public DataType MyType { get; set; }

        public bool IsUnidimentional { get; set; }

        public bool IsBidimentional { get; set; }

        public override Value GetDefaultValue()
        {
            return new IntValue();
        }
    }
}
