﻿using RoJoCompiler.Semantic;
using RoJoCompiler.Semantic.Types;
using RoJoCompiler.SyntacticTree.Accessor;
using RoJoCompiler.SyntacticTree.Statement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Expression.Identifier
{
    public class IdArrayNode : StatementNode
    {
        public GeneralDeclarationNode GeneralDeclaration { get; set; }

        public AccessorNode Unidimensional { get; set; }

        public AccessorNode Bidimensional { get; set; }

        public List<ExpressionNode> Values { get; set; }

        public override void ValidateSemantic()
        {
            var myArray = new ArrayType();
            myArray.MyType = GetType(GeneralDeclaration.DataType);

            if(Unidimensional != null)
            {
                myArray.IsUnidimentional = true;
                Unidimensional.ValidateSemantic();
            }

            if(Bidimensional != null)
            {
                myArray.IsBidimentional = true;
                Bidimensional.ValidateSemantic();
            }

            if(Values != null)
            {
                foreach (var value in Values)
                {
                    var expressionType = value.EvaluateSemantic();
                    if(expressionType.GetType() != myArray.MyType.GetType())
                        throw new SemanticException("Could not assign type " + expressionType.GetType() + " to type " + myArray.MyType.GetType() + " in row " + CurrentToken.Row + " and column " + CurrentToken.Column + " already exists.");
                }
            }

            if(StackTables.StackInstance.ExistVariableInStack(GeneralDeclaration.Id.Name))
                throw new SemanticException("Variable " + GeneralDeclaration.Id.Name + " in row " + CurrentToken.Row + " and column " + CurrentToken.Column + " already exists.");
            else
            {
                StackTables.StackInstance.Stack.Peek().DeclareVariable(GeneralDeclaration.Id.Name, myArray);
            }
        }

        public override void Interpret()
        {
            throw new NotImplementedException();
        }

        private DataType GetType(string type)
        {
            switch (type)
            {
                case "bool":
                    return new BoolType();
                case "char":
                    return new CharType();
                case "date":
                    return new DateType();
                case "enum":
                    return new EnumType();
                case "float":
                    return new FloatType();
                case "int":
                    return new IntType();
                case "struct":
                    return new StructType();
                case "string":
                    return new StringType();
                case "void":
                    return new VoidType();
                default:
                    return null;
            }
        }
    }
}
