﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class PrintfNode : StatementNode
    {
        public ExpressionNode Value { get; set; }

        public override void ValidateSemantic()
        {
            Value.EvaluateSemantic();
        }

        public override void Interpret()
        {
            var value = Value.Interpret();
            HtmlStack.StackInstance.Stack.Add(new StringValue { MyValue = this.GetValue(value) });
        }

        public string GetValue(Value value)
        {
            string x = "";
            if (value is BoolValue)
                x += ((BoolValue)value).MyValue;

            if (value is CharValue)
                x += ((CharValue)value).MyValue;

            if (value is DateValue)
                x += ((DateValue)value).MyValue;

            if (value is FloatValue)
                x += ((FloatValue)value).MyValue;

            if (value is IntValue)
                x += ((IntValue)value).MyValue;

            if (value is StringValue)
                x += ((StringValue)value).MyValue;
            return x;
        }
        
    }
}
