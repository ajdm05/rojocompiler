﻿using RoJoCompiler.Interpretation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Semantic.Types
{
    public class CharType : DataType
    {
        public override Value GetDefaultValue()
        {
            return new CharValue { MyValue = ' ' };
        }

        public override string ToString()
        {
            return "char";
        }
    }
}
