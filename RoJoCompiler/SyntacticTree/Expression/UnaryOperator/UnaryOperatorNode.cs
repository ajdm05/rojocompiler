﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic.Types;
using RoJoCompiler.SyntacticTree.Statement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Expression.UnaryOperator
{
    public class UnaryOperatorNode : ExpressionNode
    {
        public ExpressionNode Operand { get; set; }

        public override DataType EvaluateSemantic()
        {
            throw new NotImplementedException();
        }

        public override Value Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
