﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Expression.Literal
{
    public class OctalLiteralNode : ExpressionNode
    {
        public int Value { get; set; }

        public override DataType EvaluateSemantic()
        {
            return new OctalLiteral();
        }

        public override Value Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
