﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Interpretation
{
    public class DateValue : Value
    {
        public DateTime MyValue { get; set; }

        public override Value Clone()
        {
            return new DateValue { MyValue = this.MyValue };
        }
    }
}
