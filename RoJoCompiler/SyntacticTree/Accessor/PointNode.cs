﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoJoCompiler.SyntacticTree.Expression.Identifier;

namespace RoJoCompiler.SyntacticTree.Accessor
{
    public class PointNode : AccessorNode
    {
        public IdNode Id { get; set; }
    }
}
