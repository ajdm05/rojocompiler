﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Interpretation
{
    public abstract class Value
    {
        public abstract Value Clone();
    }
}
