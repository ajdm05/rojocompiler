﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Lexicon
{
    public enum TokenType
    {
        ArithmeticAdd,
        ArithmeticSubstract,
        ArithmeticMultiplication,
        ArithmeticDivision,
        ArithemeticIncrement,
        ArithmeticDecrement,
        ArithmeticMod,
        
        Assignment,
        AssignmentAdd,
        AssignmentSubstract,
        AssignmentMultiplication,
        AssignmentDivision,
        AssignmentMod,
        AssignmentRightShiftBits,
        AssignmentLeftShiftBits,
        AssignmentAnd,
        AssignmentXor,
        AssignmentOr,

        BinaryAnd,
        BinaryComplement,
        BinaryOr,
        BinaryXor,
        BinaryLeftShift,
        BinaryRightShift,

        Comma,
        Comment,
        CommentBlock,
        Colon,
        CurlyBraceLeft,
        CurlyBraceRight,

        EndOfFile,
        EndOfSentence,
        Html,
        Id,

        KeywordWhile,
        KeywordVolatile,
        KeywordVoid,
        KeywordUnsigned,
        KeywordTypedef,
        KeywordTrue,
        KeywordSwitch,
        KeywordStruct,
        KeywordString,
        KeywordStatic,
        KeywordSizeof,
        KeywordSigned,
        KeywordShort,
        KeywordRegister,
        KeywordReturn,
        KeywordPrintf,
        KeywordLong,
        KeywordInclude,
        KeywordInt,
        KeywordIf,
        KeywordGoto,
        KeywordFloat,
        KeywordFor,
        KeywordFalse,
        KeywordExtern,
        KeywordEnum,
        KeywordElse,
        KeywordDouble,
        KeywordDo,
        KeywordDefault,
        KeywordDate,
        KeywordContinue,
        KeywordConst,
        KeywordChar,
        KeywordCase,
        KeywordBreak,
        KeywordBool,
        KeywordAuto,

        LiteralString,
        LiteralChar,
        LiteralDate,

        LogicalOr,
        LogicalAnd,
        LogicalNot,
        
        Number,
        NumberHexadecimal,
        NumberOctal,
        NumberFloat,
        
        OrExclusivePerBit,

        ParenthesisLeft,
        ParenthesisRight,
        Point,
        Pointer,

        QuestionMark,

        RelationalGreaterThan,
        RelationalLessThan,
        RelationalGreaterOrEqualThan,
        RelationalLessOrEqualThan,
        RelationalDifferentThan,
        RelationalEqualTo,

        Sharp,
        SquareBracketLeft,
        SquareBracketRight
    }
}
