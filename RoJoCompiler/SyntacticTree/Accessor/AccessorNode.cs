﻿using RoJoCompiler.SyntacticTree.Statement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Accessor
{
    public class AccessorNode : StatementNode
    {
        public StatementNode LeftOperand { get; set; }

        public StatementNode RighttOperand { get; set; }

        public override void ValidateSemantic()
        {
            
        }

        public override void Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
