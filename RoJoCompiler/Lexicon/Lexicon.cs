﻿
using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoJoCompiler.Lexicon
{
    public class Lexicon
    {
        private string _sourceCode;
        private int _actualColumn;
        private int _actualRow;
        private int _cursor;
        private bool _isHtmlContent;
        private Dictionary<string, TokenType> _keywords;
        private Dictionary<string, TokenType> _generalSymbols;
        private Dictionary<string, TokenType> _arithmeticSymbols;
        private Dictionary<string, TokenType> _assigmentOperationSymbols;
        private Dictionary<string, TokenType> _relationalSymbols;
        private Dictionary<string, TokenType> _logicalSymbols;
        private Dictionary<string, TokenType> _binarySymbols;

        public Lexicon(string _sourceCode)
        {
            this._sourceCode = _sourceCode;
            this._actualColumn = 0;
            this._actualRow = 1;
            this._cursor = 0;
            this._isHtmlContent = true;
            this._keywords = DictionariesFactory.GetReservedKeywordsDictionary();
            this._generalSymbols = DictionariesFactory.GetGeneralSymbolsDictionary();
            this._arithmeticSymbols = DictionariesFactory.GetArithmeticSymbolsDictionary();
            this._assigmentOperationSymbols = DictionariesFactory.GetAssigmentSymbolsDictionary();
            this._relationalSymbols = DictionariesFactory.GetRelationalSymbolsDictionary();
            this._logicalSymbols = DictionariesFactory.GetLogicalSymbolsDictionary();
            this._binarySymbols = DictionariesFactory.GetBinarySymbolsDictionary();
        }

        public Token GetNextToken()
        {
            char temporarySymbol = GetCurrentSymbol();
            string lexema = "";

            if (temporarySymbol == CommonCharacters.EndOfFile)
            {
                return new Token { TokenType = TokenType.EndOfFile };
            }

            if (_isHtmlContent)
            {
                return this.GetHtmlContent(lexema);
            }
            else
            {
                while (char.IsWhiteSpace(temporarySymbol))
                {
                    _cursor++;
                    if (temporarySymbol == CommonCharacters.Enter)
                    {
                        _actualRow++;
                        _actualColumn = 0;
                    }
                    temporarySymbol = GetCurrentSymbol();
                }

                if (temporarySymbol == CommonCharacters.EndOfFile)
                {
                    return new Token { TokenType = TokenType.EndOfFile };
                }

                if (char.IsLetter(temporarySymbol) || temporarySymbol == CommonCharacters.LowDash)
                {
                    lexema += temporarySymbol;
                    _cursor++;
                    return GetIdentifier(lexema);
                }

                if (char.IsDigit(temporarySymbol))
                {
                    lexema += temporarySymbol;
                    _cursor++;

                    if (temporarySymbol == CommonCharacters.Zero)
                    {
                        var currentSymbol = GetCurrentSymbol();
                        //aqui
                        if(currentSymbol != CommonCharacters.EndOfSentence)
                            lexema += currentSymbol;
                        if (currentSymbol == CommonCharacters.X)
                        {
                            return this.GetHexadecimalDigit(lexema);
                        }
                        else if (char.IsDigit(currentSymbol))
                        {
                            return this.GetOctalDigit(lexema);
                        }
                    }

                    var number = this.GetDigit(lexema);
                    temporarySymbol = GetCurrentSymbol();
                    if (temporarySymbol == CommonCharacters.Point)
                    {
                        lexema = number.Lexema;
                        lexema += temporarySymbol;
                        _cursor++;
                        return this.GetFloatDigit(lexema);
                    }

                    if (char.ToUpper(temporarySymbol) == CommonCharacters.E)
                    {
                        var ex = temporarySymbol;
                        _cursor++;
                        Token expNumber = GetDigit(string.Empty);
                        temporarySymbol = GetCurrentSymbol();
                        if (!char.IsLetter(temporarySymbol) && temporarySymbol == CommonCharacters.EndOfSentence || char.IsWhiteSpace(temporarySymbol))
                            return this.MakeTokenToReturn(number.Lexema + ex + expNumber.Lexema, TokenType.NumberFloat);
                    }

                    return number;
                }

                if (_relationalSymbols.ContainsKey(temporarySymbol.ToString()))
                {
                    lexema += temporarySymbol;
                    _cursor++;
                    return this.GetRelationalSymbol(lexema);
                }

                if (_generalSymbols.ContainsKey(temporarySymbol.ToString()))
                {
                    lexema += temporarySymbol;
                    _cursor++;
                    return this.GetGeneralSymbol(lexema);
                }

                if (_assigmentOperationSymbols.ContainsKey(temporarySymbol.ToString()))
                {
                    lexema += temporarySymbol;
                    _cursor++;
                    return this.GetAssigmentSymbol(lexema);
                }

                if (_binarySymbols.ContainsKey(temporarySymbol.ToString()))
                {
                    lexema += temporarySymbol;
                    _cursor++;
                    return this.GetBitwiseSymbol(lexema);
                }

                if (_arithmeticSymbols.ContainsKey(temporarySymbol.ToString()))
                {
                    lexema += temporarySymbol;
                    _cursor++;
                    return this.GetArithmeticSymbol(lexema);
                }

                if (_logicalSymbols.ContainsKey(temporarySymbol.ToString()))
                {
                    lexema += temporarySymbol;
                    _cursor++;

                    return this.GetLogicalSymbol(lexema);
                }

                if (temporarySymbol == CommonCharacters.Apostrophe)
                    return this.GetLiteralChar();


                if (temporarySymbol == CommonCharacters.DittoMark)
                    return this.GetLiteralString();

                throw new LexiconException("Found unexpected symbol.");
            }
        }

        private Token GetHtmlContent(string lexema)
        {
            if (lexema != "\0")
            {
                while (_isHtmlContent)
                {
                    var temporarySymbol = GetCurrentSymbol();
                    if (temporarySymbol != '\0')
                    {
                        _cursor++;
                        var nextTemporarySymbol = GetCurrentSymbol();
                        if (temporarySymbol == CommonCharacters.LessThan && nextTemporarySymbol == CommonCharacters.Mod)
                        {
                            _cursor++;
                            _isHtmlContent = false;
                        }
                        else
                        {
                            lexema += temporarySymbol;
                        }
                    }
                    else
                    {
                        //HtmlStack.StackInstance.Stack.Add(new StringValue { MyValue = lexema });
                        return this.MakeTokenToReturn(lexema, TokenType.EndOfFile);
                        //return this.GetNextToken();
                    }
                }

                return this.MakeTokenToReturn(lexema, TokenType.Html);
            }
            return this.MakeTokenToReturn(lexema, TokenType.EndOfFile);
        }

        private Token GetHexadecimalDigit(string lexema)
        {
            _cursor++;
            var temporarySymbol = GetCurrentSymbol();
            
            while(char.IsDigit(temporarySymbol) || char.ToUpper(temporarySymbol) == CommonCharacters.A || char.ToUpper(temporarySymbol) == CommonCharacters.B || 
                    char.ToUpper(temporarySymbol) == CommonCharacters.C || char.ToUpper(temporarySymbol) == CommonCharacters.D || char.ToUpper(temporarySymbol) == CommonCharacters.E || 
                    char.ToUpper(temporarySymbol) == CommonCharacters.F)
            {
                lexema += temporarySymbol;
                _cursor++;
                temporarySymbol = GetCurrentSymbol();
            }

            if (char.IsLetter(temporarySymbol) || temporarySymbol != CommonCharacters.EndOfSentence && !char.IsWhiteSpace(temporarySymbol))
                throw new LexiconException("It is not an hexadecimal valid format.");

            return this.MakeTokenToReturn(lexema, TokenType.NumberHexadecimal);
        }

        private Token GetOctalDigit(string lexema)
        {
            _cursor++;
            var temporarySymbol = GetCurrentSymbol();
            while (temporarySymbol == CommonCharacters.Zero || temporarySymbol == CommonCharacters.One || temporarySymbol == CommonCharacters.Two || temporarySymbol == CommonCharacters.Three ||
                temporarySymbol == CommonCharacters.Four || temporarySymbol == CommonCharacters.Five || temporarySymbol == CommonCharacters.Six || temporarySymbol == CommonCharacters.Seven)
            {
                lexema += temporarySymbol;
                _cursor++;
                temporarySymbol = GetCurrentSymbol();
            }

            if (char.IsLetter(temporarySymbol) || temporarySymbol != CommonCharacters.EndOfSentence && !char.IsWhiteSpace(temporarySymbol) || 
                (char.IsDigit(temporarySymbol) && Int32.Parse(temporarySymbol.ToString()) > 7))
                throw new LexiconException("It is not an octal valid format."); ;

            return this.MakeTokenToReturn(lexema, TokenType.NumberOctal);
        }

        private Token GetFloatDigit(string lexema)
        {
            Token decimalNumber = this.GetDigit(string.Empty);
            if (decimalNumber.Lexema != string.Empty)
            {
                var temporarySymbol = GetCurrentSymbol();
                if (char.ToUpper(temporarySymbol) == CommonCharacters.E)
                {
                    decimalNumber.Lexema += temporarySymbol;
                    _cursor++;
                    Token expoNumber = this.GetDigit(string.Empty);
                    decimalNumber.Lexema += expoNumber.Lexema;
                }
                temporarySymbol = GetCurrentSymbol();

                if (!char.IsLetter(temporarySymbol))
                    return this.MakeTokenToReturn(lexema + decimalNumber.Lexema, TokenType.NumberFloat);
            }

            throw new LexiconException("It is not a valid float format.");
        }

        private Token GetDigit(string lexema)
        {
            var temporarySymbol = GetCurrentSymbol();
            while (char.IsDigit(temporarySymbol))
            {
                lexema += temporarySymbol;
                _cursor++;
                temporarySymbol = GetCurrentSymbol();
            }

            return this.MakeTokenToReturn(lexema, TokenType.Number);
        }

        private Token GetIdentifier(string lexema)
        {
            var temporarySymbol = GetCurrentSymbol();
            while (char.IsLetterOrDigit(temporarySymbol) || temporarySymbol == '_')
            {
                lexema += temporarySymbol;
                _cursor++;
                temporarySymbol = GetCurrentSymbol();
            }

            var tokenType = _keywords.ContainsKey(lexema) ? _keywords[lexema] : TokenType.Id;
            return this.MakeTokenToReturn(lexema, tokenType);
        }

        private Token GetRelationalSymbol(string lexema)
        {
            if (lexema == ">" || lexema == "<")
            {
                var temporarySymbol = this.GetCurrentSymbol();
                if (lexema == "<" && temporarySymbol == CommonCharacters.Mod)
                {
                    _cursor++;
                    _isHtmlContent = false;
                    return this.GetNextToken();
                } 
                else
                {
                    if (temporarySymbol == CommonCharacters.GreaterThan || temporarySymbol == CommonCharacters.LessThan)
                    {
                        lexema += temporarySymbol;
                        _cursor++;
                        var nextTemporarySymbol = this.GetCurrentSymbol();
                        if (nextTemporarySymbol == CommonCharacters.Assignment)
                        {
                            lexema += nextTemporarySymbol;
                            _cursor++;
                            return this.GetAssigmentSymbol(lexema);
                        }
                        return this.GetBitwiseSymbol(lexema);
                    }
                    else if (temporarySymbol == CommonCharacters.Assignment)
                    {
                        lexema += temporarySymbol;
                        _cursor++;
                    }
                }
            }
            return this.MakeTokenToReturn(lexema, _relationalSymbols[lexema]);
        }

        private Token GetLogicalSymbol(string lexema)
        {
            if (lexema == "!")
            {
                var nextTemporarySymbol = this.GetCurrentSymbol();
                if (nextTemporarySymbol == CommonCharacters.Assignment)
                {
                    lexema += nextTemporarySymbol;
                    _cursor++;
                    return this.GetRelationalSymbol(lexema);
                }
            }

            return this.MakeTokenToReturn(lexema, _logicalSymbols[lexema]);
        }

        private Token GetGeneralSymbol(string lexema)
        {
            var temporarySymbol = GetCurrentSymbol();

            var generalSymbol = this.GetTokenFromDictionary(_relationalSymbols, lexema + temporarySymbol);
            if (generalSymbol != null)
                return generalSymbol;

            var logicSymbolsToken = GetTokenFromDictionary(_logicalSymbols, lexema + temporarySymbol);
            if (logicSymbolsToken != null)
                return logicSymbolsToken;

            if (lexema[0] == CommonCharacters.Sharp && char.IsNumber(temporarySymbol))
            {
                lexema += temporarySymbol;
                _cursor++;
                return this.GetLiteralDate(lexema);
            }

            if (lexema[0] == CommonCharacters.Sharp && temporarySymbol == CommonCharacters.LowerI)
            {
                lexema += temporarySymbol;
                _cursor++;
                return this.GetInclude(lexema);
            }

            if (_generalSymbols.ContainsKey(lexema))
                return this.MakeTokenToReturn(lexema, _generalSymbols[lexema]);

            return null;
        }

        private Token GetInclude(string lexema)
        {
             var temporalCursor = _cursor;
             if (_sourceCode[temporalCursor] == CommonCharacters.LowerN && _sourceCode[temporalCursor + 1] == CommonCharacters.LowerC && _sourceCode[temporalCursor + 2] == CommonCharacters.LowerL && 
                 _sourceCode[temporalCursor + 3] == CommonCharacters.LowerU && _sourceCode[temporalCursor + 4] == CommonCharacters.LowerD && _sourceCode[temporalCursor + 5] == CommonCharacters.LowerE)
            {
                for(int i = 0; i < 6; i++){
                    lexema += _sourceCode[_cursor + i]; 
                }

                _cursor += 6;
                _actualColumn += 6;
                var temporarySymbol = GetCurrentSymbol();
                if (!char.IsLetter(temporarySymbol) && temporarySymbol == CommonCharacters.EndOfSentence || char.IsWhiteSpace(temporarySymbol))
                    return this.MakeTokenToReturn(lexema, TokenType.KeywordInclude);
            }

            throw new LexiconException("It is not a valid include format.");
        }

        private Token GetLiteralDate(string lexema)
        {
            var temporalCursor = _cursor;
            if (char.IsNumber(_sourceCode[temporalCursor]) && _sourceCode[temporalCursor + 1] == CommonCharacters.MiddleDash && char.IsNumber(_sourceCode[temporalCursor + 2]) && char.IsNumber(_sourceCode[temporalCursor + 3])
                && _sourceCode[temporalCursor + 4] == CommonCharacters.MiddleDash && char.IsNumber(_sourceCode[temporalCursor + 5]) && char.IsNumber(_sourceCode[temporalCursor + 6]) && 
                char.IsNumber(_sourceCode[temporalCursor + 7]) && char.IsNumber(_sourceCode[temporalCursor + 8]) && _sourceCode[temporalCursor + 9] == CommonCharacters.Sharp)
            {
                for(int i = 0; i < 10; i++){
                    lexema += _sourceCode[_cursor + i]; 
                }

                _cursor += 10;
                _actualColumn += 10;
                var temporarySymbol = GetCurrentSymbol();
                if (!char.IsLetter(temporarySymbol) && temporarySymbol == CommonCharacters.EndOfSentence || char.IsWhiteSpace(temporarySymbol))
                    return this.MakeTokenToReturn(lexema, TokenType.LiteralDate);
            }

            throw new LexiconException("It is not a valid date format.");
        }

        private Token GetArithmeticSymbol(string lexema)
        {
            var temporarySymbol = this.GetCurrentSymbol();
            if (lexema == "%" && temporarySymbol == CommonCharacters.GreaterThan)
            {
                _cursor++;
                _isHtmlContent = true;
                return this.GetNextToken();
            }
            else
            {
                var arithmeticSymbol = this.GetTokenFromDictionary(_arithmeticSymbols, lexema + temporarySymbol);

                if (arithmeticSymbol != null)
                    return arithmeticSymbol;

                var assigmentSymbol = GetTokenFromDictionary(_assigmentOperationSymbols, lexema + temporarySymbol);
                if (assigmentSymbol != null)
                    return assigmentSymbol;

                var generalSymbol = GetTokenFromDictionary(_generalSymbols, lexema + temporarySymbol);
                if (generalSymbol != null)
                    return generalSymbol;

                if (lexema[0] == CommonCharacters.Slash && temporarySymbol == CommonCharacters.Slash)
                {
                    this.ConsumeSingleLineComment();
                    return this.GetNextToken();
                }

                if (lexema[0] == CommonCharacters.Slash && temporarySymbol == CommonCharacters.Asterisk)
                {
                    this.ConsumeMultiLineComment();
                    _cursor++;
                    return this.GetNextToken();
                }

                return this.MakeTokenToReturn(lexema, _arithmeticSymbols[lexema]);
            }
        }

        private Token GetAssigmentSymbol(string lexema)
        {
            if (lexema == "=")
            {
                var nextTemporarySymbol = this.GetCurrentSymbol();
                if (nextTemporarySymbol == CommonCharacters.Assignment)
                {
                    lexema += nextTemporarySymbol;
                    _cursor++;
                    return this.GetRelationalSymbol(lexema);
                }
            }

            return this.MakeTokenToReturn(lexema, _assigmentOperationSymbols[lexema]);
        }

        private Token GetBitwiseSymbol(string lexema)
        {
            if (lexema == "&" || lexema == "|" || lexema == "^")
            {
                var nextTemporarySymbol = this.GetCurrentSymbol();
                if (nextTemporarySymbol == CommonCharacters.BitwiseAnd || nextTemporarySymbol == CommonCharacters.BitwiseOr)
                {
                    lexema += nextTemporarySymbol;
                    _cursor++;
                    return this.GetLogicalSymbol(lexema);
                }
 
                if (nextTemporarySymbol == CommonCharacters.Assignment)
                {
                    lexema += nextTemporarySymbol;
                    _cursor++;
                    return this.GetAssigmentSymbol(lexema);
                } 
            }
            
            var temporarySymbol = this.GetCurrentSymbol();
            var assigmentSymbol = GetTokenFromDictionary(_assigmentOperationSymbols, lexema + temporarySymbol);
            if (assigmentSymbol != null)
                return assigmentSymbol;

            var generalSymbol = GetTokenFromDictionary(_generalSymbols, lexema + temporarySymbol);
            if (generalSymbol != null)
                return generalSymbol;

            if (lexema[0] == CommonCharacters.Slash && temporarySymbol == CommonCharacters.Slash)
            {
                this.ConsumeSingleLineComment();
                return this.GetNextToken();
            }

            if (lexema[0] == CommonCharacters.Slash && temporarySymbol == CommonCharacters.Asterisk)
            {
                this.ConsumeMultiLineComment();
                _cursor++;
                return this.GetNextToken();
            }

            return this.MakeTokenToReturn(lexema, _binarySymbols[lexema]);
        }

        private void ConsumeSingleLineComment()
        {
            _cursor++;
            var temporarySymbol = this.GetCurrentSymbol();
            while (temporarySymbol != CommonCharacters.Enter && temporarySymbol != CommonCharacters.EndOfFile)
            {
                _cursor++;
                temporarySymbol = this.GetCurrentSymbol();
            }
        }

        private void ConsumeMultiLineComment()
        {
            _cursor++;
            var temporarySymbol = this.GetCurrentSymbol();
            while (temporarySymbol != CommonCharacters.EndOfFile)
            {
                _cursor++;
                if (temporarySymbol == CommonCharacters.Asterisk && this.GetCurrentSymbol() == CommonCharacters.Slash){
                    _actualColumn = 0;
                    break;
                }  
                else
                    temporarySymbol = this.GetCurrentSymbol();
            }

            if (temporarySymbol == CommonCharacters.EndOfFile)
                throw new LexiconException("Multiline comment was never closed.");
        }

        private Token GetTokenFromDictionary(Dictionary<string, TokenType> dictionary, string symbol)
        {
            Token tokenToReturn = null;
            if (dictionary.ContainsKey(symbol))
            {
                _cursor++;
                tokenToReturn = this.MakeTokenToReturn(symbol, dictionary[symbol]);
            }

            return tokenToReturn;
        }

        private Token GetLiteralString()
        {
            string text = "";
            _cursor++;
            var temporalSymbol = GetCurrentSymbol();
            while (temporalSymbol != CommonCharacters.DittoMark && temporalSymbol != CommonCharacters.EndOfFile)
            {
                text += temporalSymbol;
                _cursor++;
                temporalSymbol = this.GetCurrentSymbol();
            }

            if (temporalSymbol == CommonCharacters.DittoMark)
            {
                _cursor++;
                temporalSymbol = this.GetCurrentSymbol();
                return this.MakeTokenToReturn(text, TokenType.LiteralString);
            }

            throw new LexiconException("The string was never closed.");
        }

        private Token GetLiteralChar()
        {
            var text = "";
            _cursor++;
            var temporarySymbol = GetCurrentSymbol();

            while (temporarySymbol != CommonCharacters.Apostrophe)
            {
                text += temporarySymbol;
                _cursor++;
                temporarySymbol = this.GetCurrentSymbol();

                if (temporarySymbol == CommonCharacters.Apostrophe)
                {
                    _cursor++;
                    var nextTemporarySymbol = this.GetCurrentSymbol();
                    if (nextTemporarySymbol == CommonCharacters.Apostrophe)
                        text += temporarySymbol;
                }
            }

            if (text.Length == 1)
                return this.MakeTokenToReturn(text, TokenType.LiteralChar);
            

            if (text.Length == 2)
            {
                if (text.StartsWith("\a") || text.StartsWith("\b") || text.StartsWith("\f") || text.StartsWith("\n") || text.StartsWith("\r") ||
                    text.StartsWith("\t") || text.StartsWith("\v") || text.StartsWith("\'") || text.StartsWith("\"") || text.StartsWith("\\") || text.Equals("\0"))
                {
                    _cursor++;
                    return this.MakeTokenToReturn(text, TokenType.LiteralChar);
                }
            }

            throw new LexiconException("It is not a valid char format.");
        }

        private char GetCurrentSymbol()
        {
            if (_cursor < _sourceCode.Length)
            {
                _actualColumn++;
                return _sourceCode[_cursor];
            }
            return CommonCharacters.EndOfFile;
        }

        private Token MakeTokenToReturn(string lexema, TokenType token)
        {
            return new Token
            {
                Lexema = lexema,
                TokenType = token,
                Row = _actualRow,
                Column = _actualColumn
            };
        }
    }
}
