﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Interpretation
{
    public class BoolValue : Value
    {
        public bool MyValue { get; set; }

        public override Value Clone()
        {
            return new BoolValue { MyValue = this.MyValue };
        }
    }
}
