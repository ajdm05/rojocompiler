﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Expression.Literal
{
    public class DateLiteralNode : ExpressionNode
    {
        public DateTime Value { get; set; }

        public override DataType EvaluateSemantic()
        {
            return new DateType();
        }

        public override Value Interpret()
        {
            return new DateValue { MyValue = this.Value };
        }
    }
}
