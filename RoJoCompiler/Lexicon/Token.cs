﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoJoCompiler.Lexicon
{
    public class Token
    {
        public string Lexema { get; set; }

        public TokenType TokenType { get; set; }

        public int Row { get; set; }

        public int Column { get; set; }

        public override string ToString()
        {
            return "Lexema: " + Lexema + " Type: " + TokenType + " Row: " + Row + " Column: " + Column;
        }
    }
}
