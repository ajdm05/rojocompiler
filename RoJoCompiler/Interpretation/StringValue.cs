﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Interpretation
{
    public class StringValue : Value 
    {
        public string MyValue { get; set; }

        public override Value Clone()
        {
            return new StringValue { MyValue = this.MyValue };
        }
    }
}
