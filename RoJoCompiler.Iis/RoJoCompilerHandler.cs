﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic.Types;
using RoJoCompiler.Syntactic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;

namespace RoJoCompiler.Iis
{
    public class RoJoCompilerHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            var stringBuilder = new StringBuilder();
            string apPath = HttpContext.Current.Server.MapPath("~/RoJoCompiler.webc");
            System.IO.StreamReader file = new StreamReader(apPath);
            var sourceCode = file.ReadToEnd().ToString();
            var lexicon = new Lexicon.Lexicon(sourceCode);

            Parser parser = new Parser(lexicon);
            try
            {
                HtmlStack.StackInstance.Stack.Clear();
                List<Value> stack = null;
                var expressions = parser.Parse();
                foreach (var statements in expressions)
                {
                    if(statements != null)
                        statements.ValidateSemantic();
                }

                foreach (var statements in expressions)
                {
                    if (statements != null)
                        statements.Interpret();
                }

                if (HtmlStack.StackInstance.LastHtml != null)
                    HtmlStack.StackInstance.Stack.Add(HtmlStack.StackInstance.LastHtml);

                stack = HtmlStack.StackInstance.Stack;
                string x = "";
                foreach (var s in stack)
                {
                    if (s is BoolValue)
                        x += ((BoolValue)s).MyValue;

                    if (s is CharValue)
                        x += ((CharValue)s).MyValue;

                    if (s is DateValue)
                        x += ((DateValue)s).MyValue;

                    if (s is FloatValue)
                        x += ((FloatValue)s).MyValue;

                    if (s is IntValue)
                        x += ((IntValue)s).MyValue;

                    if (s is StringValue)
                        x += ((StringValue)s).MyValue;
                }

                stringBuilder.Clear();
                stringBuilder.Append(x);
                var html = stringBuilder.ToString();
                HttpContext.Current.Response.ContentType = "text/html; charset=utf-8";
                HttpContext.Current.Response.ContentEncoding = Encoding.UTF8;
                HttpContext.Current.Response.Write(html);
                if(this.GetMethod() == "POST")
                {
                    var myParams = this.GetForm(context, "display");
                    var newlexicon = new Lexicon.Lexicon("<% int a = " + myParams + "; printf(a);%>" + '\0');
                    var newparser = new Parser(newlexicon);
                    HtmlStack.StackInstance.Stack.Clear();
                    var newexpressions = newparser.Parse();

                    foreach(var ep in newexpressions){


                    }

                    foreach (var statements in newexpressions)
                    {
                        if (statements != null)
                            statements.ValidateSemantic();
                    }

                    foreach (var statements in newexpressions)
                    {
                        if (statements != null)
                            statements.Interpret();
                    }

                    stack = HtmlStack.StackInstance.Stack;
                    x = "";
                    foreach (var s in stack)
                    {
                        if (s is BoolValue)
                            x += ((BoolValue)s).MyValue;

                        if (s is CharValue)
                            x += ((CharValue)s).MyValue;

                        if (s is DateValue)
                            x += ((DateValue)s).MyValue;

                        if (s is FloatValue)
                            x += ((FloatValue)s).MyValue;

                        if (s is IntValue)
                            x += ((IntValue)s).MyValue;

                        if (s is StringValue)
                            x += ((StringValue)s).MyValue;
                    }

                    stringBuilder.Clear();
                    stringBuilder.Append("<div> Result: " + x + "</div>");
                    var h = stringBuilder.ToString();
                    HttpContext.Current.Response.Write(h);
                }
                
            }
            catch (ParserException ex)
            {
                stringBuilder.Append(ex.Message);
                var html = stringBuilder.ToString();
                stringBuilder.Append("</b></body></html>");
                HttpContext.Current.Response.ContentType = "text/html; charset=utf-8";
                HttpContext.Current.Response.ContentEncoding = Encoding.UTF8;
                HttpContext.Current.Response.Write(html);
                //context.Response.Write(html);
            }
        }

        public string GetQueryString(HttpContext context, string query)
        {
            return context.Request.QueryString[query];
        }

        public string GetForm(HttpContext context, string form)
        {
            return context.Request.Form[form];
        }

        public string GetMethod()
        {
            return HttpContext.Current.Request.HttpMethod;
        }
    }
}
