﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic;
using RoJoCompiler.Semantic.Types;
using RoJoCompiler.SyntacticTree.Expression.Identifier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Expression.BinaryOperator
{
    public class AdditionAssignmentNode : BinaryOperatorNode
    {
        public override DataType EvaluateSemantic()
        {
            if(!(LeftOperand is IdNode))
                throw new SemanticException("Literals could not be assigned in row " + CurrentToken.Row + " column " + CurrentToken.Column + ".");

            var rightType = RightOperand.EvaluateSemantic();
            var idNode = ((IdNode)LeftOperand);
            if (string.IsNullOrEmpty(idNode.DataType))
            {
                var leftType = idNode.EvaluateSemantic();
                if(leftType is VoidType || rightType is VoidType || leftType is EnumType || rightType is EnumType || leftType is DateType ||
                    rightType is DateType || leftType is StructType || rightType is StructType)
                    throw new SemanticException("Could not Add " + rightType + " with " + leftType + " in row " + CurrentToken.Row + " column " + CurrentToken.Column + ".");

                if((leftType is StringType && rightType is IntType) || (leftType == rightType))
                    return leftType;

                if ((rightType is CharType && leftType is IntType) || (leftType is IntType && rightType is CharType))
                    return new IntType();

                if ((rightType is FloatType && leftType is IntType) || (leftType is IntType && rightType is FloatType))
                    return new FloatType();

                throw new SemanticException("Could not Add " + rightType + " with " + leftType + " in row " + CurrentToken.Row + " column " + CurrentToken.Column + ".");
            }
            else if (!StackTables.StackInstance.ExistVariableInStack(idNode.Name))
            {
                var idType = this.GetType(idNode.DataType);
                StackTables.StackInstance.Stack.Peek().DeclareVariable(idNode.Name, idType);
                return idType;
            }
            else
            {
                throw new SemanticException("Variable " + idNode.Name + " in row " + CurrentToken.Row + " and column " + CurrentToken.Column + " already exists.");
            }
        }

        public override Value Interpret()
        {
            return null;
        }

        private DataType GetType(string type)
        {
            switch (type)
            {
                case "bool":
                    return new BoolType();
                case "char":
                    return new CharType();
                case "date":
                    return new DateType();
                case "enum":
                    return new EnumType();
                case "float":
                    return new FloatType();
                case "int":
                    return new IntType();
                case "struct":
                    return new StructType();
                case "string":
                    return new StringType();
                case "void":
                    return new VoidType();
                default:
                    return null;
            }
        }
    }
}
