﻿using RoJoCompiler.Interpretation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Semantic.Types
{
    public class HtmlStack
    {
        private static HtmlStack _stackInstance { get; set; }

        public Value LastHtml { get; set; }

        public List<Value> Stack { get; set; }

        private HtmlStack()
        {
            Stack = new List<Value>();
        }

        public static HtmlStack StackInstance 
        {
            get
            {
                if (_stackInstance == null)
                    _stackInstance = new HtmlStack();
                return _stackInstance;
            }
        }
    }
}
