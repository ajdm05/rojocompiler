﻿using RoJoCompiler.SyntacticTree.Statement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RoJoCompiler.SyntacticTree.Accessor;
using RoJoCompiler.Semantic.Types;
using RoJoCompiler.Semantic;

namespace RoJoCompiler.SyntacticTree.Expression.Identifier
{
    public class IdStatementNode : StatementNode
    {
        public string DataType { get; set; }

        public string Name { get; set; }

        public List<ExpressionNode> Pointers { get; set; }

        public List<AccessorNode> Accessors { get; set; }

        public ExpressionNode InitializedValue { get; set; }

        public override void ValidateSemantic()
        {
            if (!SymbolsTable.Instance.ExistVariable(Name))
                throw new SemanticException("Variable " + Name + " does not exist.");
            //return SymbolsTable.Instance.GetVariable(Name);
        }

        public override void Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
