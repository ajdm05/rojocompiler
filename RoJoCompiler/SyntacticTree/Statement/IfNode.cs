﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class IfNode : StatementNode
    {
        public ExpressionNode BooleanCondition { get; set; }

        public List<StatementNode> IfBlock { get; set; }

        public List<StatementNode> ElseBlock { get; set; }

        public override void ValidateSemantic()
        {
            throw new NotImplementedException();
        }

        public override void Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
