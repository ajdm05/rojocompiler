﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Lexicon;
using RoJoCompiler.Semantic.Types;
using RoJoCompiler.SyntacticTree;
using RoJoCompiler.SyntacticTree.Accessor;
using RoJoCompiler.SyntacticTree.Expression.BinaryOperator;
using RoJoCompiler.SyntacticTree.Expression.Identifier;
using RoJoCompiler.SyntacticTree.Expression.Literal;
using RoJoCompiler.SyntacticTree.Expression.UnaryOperator;
using RoJoCompiler.SyntacticTree.Statement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoJoCompiler.Syntactic
{
    public class Parser
    {
        private Lexicon.Lexicon _lexicon;
        private Token _currenToken;
        private Dictionary<string, int> _variables;
        public Parser(Lexicon.Lexicon lexer)
        {
            _lexicon = lexer;
            this.SetNextToken();
            _variables = new Dictionary<string, int>();
        }

        public List<StatementNode> Parse()
        {
            var code = Code();
            if (_currenToken.TokenType != TokenType.EndOfFile)
                throw new ParserException("EOF was expecting.");

            if (!string.IsNullOrEmpty(_currenToken.Lexema))
                HtmlStack.StackInstance.LastHtml = new StringValue { MyValue = _currenToken.Lexema };
            return code;
        }

        private List<StatementNode> Code()
        {
            return ListOfSentences();
        }

        private List<StatementNode> ListOfSentences()
        {
            //Lista_Sentencias->Sentence Lista_Sentencias
            if ( this.IsADataType(_currenToken.TokenType) || this.IsASentence(_currenToken.TokenType) || _currenToken.TokenType == TokenType.Id)
            {
                var statement = Sentence();
                var statementList = ListOfSentences();
                statementList.Insert(0, statement);
                return statementList;
            }
            //Lista_Sentencia->Epsilon
            else
            {
                return new List<StatementNode>();
            }
        }

        private List<StatementNode> ListOfSpecialSentences()
        {
            if (this.IsADataType(_currenToken.TokenType) || this.IsAnSpecialSentence(_currenToken.TokenType) || _currenToken.TokenType == TokenType.Id)
            {
                var statement = this.SpecialSentence();
                var specialStatementList = this.ListOfSpecialSentences();
                specialStatementList.Insert(0, statement);
                return specialStatementList;
            }
            else
            {
                return new List<StatementNode>();
            }
        }

        private StatementNode SpecialSentence()
        {
            if (this.IsADataType(_currenToken.TokenType))
            {
                return this.SpecialDeclaration();
            }
            else if (_currenToken.TokenType == TokenType.KeywordIf)
            {
                return this.If();
            }
            else if (_currenToken.TokenType == TokenType.KeywordWhile)
            {
                return this.While();
            }
            else if (_currenToken.TokenType == TokenType.KeywordDo)
            {
                return this.Do();
            }
            else if (_currenToken.TokenType == TokenType.KeywordFor)
            {
                return this.ForLoop();
            }
            else if (_currenToken.TokenType == TokenType.Id)
            {
                return this.AssignmentOrFunctionCall();
            }
            else if (_currenToken.TokenType == TokenType.KeywordConst)
            {
                return this.Const();
            }
            else if (_currenToken.TokenType == TokenType.KeywordSwitch)
            {
                return this.Switch();
            }
            else if (_currenToken.TokenType == TokenType.KeywordBreak)
            {
                return this.Break();
            }
            else if (_currenToken.TokenType == TokenType.KeywordContinue)
            {
                return this.Continue();
            }
            else if (_currenToken.TokenType == TokenType.KeywordInclude)
            {
                return this.Include();
            }
            else if (_currenToken.TokenType == TokenType.KeywordEnum)
            {
                return this.Enum();
            }
            else if (_currenToken.TokenType == TokenType.KeywordReturn)
            {
                return this.Return();
            }
            else if (_currenToken.TokenType == TokenType.KeywordPrintf)
            {
                return this.PrintF();
            }
            else
            {
                throw new ParserException("An Special Statement was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
            }
        }

        private StatementNode Sentence()
        {
            if (this.IsADataType(_currenToken.TokenType))
            {
                return this.Declaration();
            }
            else if (_currenToken.TokenType == TokenType.KeywordIf)
            {
                return this.If();
            }
            else if (_currenToken.TokenType == TokenType.KeywordWhile)
            {
                return this.While();
            }
            else if (_currenToken.TokenType == TokenType.KeywordDo)
            {
                return this.Do();
            }
            else if (_currenToken.TokenType == TokenType.KeywordFor)
            {
                return this.ForLoop();
            }
            else if (_currenToken.TokenType == TokenType.KeywordSwitch)
            {
                return this.Switch();
            }
            else if (_currenToken.TokenType == TokenType.Id)
            {
                return this.AssignmentOrFunctionCall();
            }
            else if (_currenToken.TokenType == TokenType.KeywordStruct)
            {
                return this.Struct();
            }
            else if (_currenToken.TokenType == TokenType.KeywordConst)
            {
                return this.Const();
            }
            else if (_currenToken.TokenType == TokenType.KeywordBreak)
            {
                return this.Break();
            }
            else if (_currenToken.TokenType == TokenType.KeywordContinue)
            {
                return this.Continue();
            }
            else if (_currenToken.TokenType == TokenType.KeywordInclude)
            {
                return this.Include();
            }
            else if (_currenToken.TokenType == TokenType.KeywordEnum)
            {
                return this.Enum();
            }
            else if(_currenToken.TokenType == TokenType.KeywordPrintf)
            {
                return this.PrintF();
            }
            else
            {
                throw new ParserException("An statement was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
            }
        }

        private PrintfNode PrintF()
        {
            if (_currenToken.TokenType != TokenType.KeywordPrintf)
                throw new ParserException("A Printf was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.ParenthesisLeft)
                throw new ParserException("A ( was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
            this.SetNextToken();
            var expressionValue = this.Expression();

            if (_currenToken.TokenType != TokenType.ParenthesisRight)
                throw new ParserException("A ) was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.EndOfSentence)
                throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
            this.SetNextToken();

            return new PrintfNode { Value = expressionValue, CurrentToken = _currenToken };
        }

        private IncludeNode Include()
        {
            if(_currenToken.TokenType != TokenType.KeywordInclude)
                throw new ParserException("An Include was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.Id)
                throw new ParserException("An LeftOperand was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            var currentToken = _currenToken;
            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.EndOfSentence)
                throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            return new IncludeNode { Root = currentToken.Lexema, CurrentToken = _currenToken };
        }

        private ContinueNode Continue()
        {
            if (_currenToken.TokenType != TokenType.KeywordContinue)
                throw new ParserException("A Continue was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.EndOfSentence)
                throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            return new ContinueNode { CurrentToken = _currenToken };
        }

        private ReturnNode Return()
        {
            if (_currenToken.TokenType != TokenType.KeywordReturn)
                throw new ParserException("A Return was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var valueToReturn = this.Expression();

            if (_currenToken.TokenType != TokenType.EndOfSentence)
                throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
            this.SetNextToken();
            return new ReturnNode { ValueToReturn = valueToReturn, CurrentToken = _currenToken };
        }

        private StatementNode AssignmentOrFunctionCall()
        {
            if (_currenToken.TokenType == TokenType.ArithmeticMultiplication)
            {
                this.SetNextToken();
            }

            if (_currenToken.TokenType != TokenType.Id)
                throw new ParserException("An Id was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            var id = new IdNode { Name = _currenToken.Lexema, CurrentToken = _currenToken };
            this.SetNextToken();
            if (this.IsArrowOrDot(_currenToken.TokenType))
            {
                var accessor = _currenToken;
                this.SetNextToken();
                var bidArray = this.BidArray();
                var values = this.ValueForPreId();

                if (_currenToken.TokenType != TokenType.EndOfSentence)
                    throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
                this.SetNextToken();
            }
            else if (this.IsDecrementOrIncrement(_currenToken.TokenType))
            {
                this.SetNextToken();
                if (_currenToken.TokenType != TokenType.EndOfSentence)
                    throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                this.SetNextToken();
                var decrementOrIncrement = this.GetPosIncrementOrDecrementNode(id.Name);
                return new AssignmentStatementNode { LeftOperand = id, RightOperand = decrementOrIncrement, CurrentToken = _currenToken };
            }
            else
            {
                var type = _currenToken;
                if (type.TokenType == TokenType.ParenthesisLeft)
                    return this.CallFunctionStatement(id);
                else if(this.IsAssignmentOperator(type.TokenType))
                {
                    this.SetNextToken();
                    var rightOperand = this.Expression();
                    var expression = this.GetAssignmentType(type);
                    expression.LeftOperand = id;
                    expression.RightOperand = rightOperand;

                    if (_currenToken.TokenType != TokenType.EndOfSentence)
                        throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                    this.SetNextToken();
                    return new AssignmentStatementNode { LeftOperand = id, RightOperand = expression, CurrentToken = _currenToken };
                }
            }
            return null;
        }

        private ExpressionNode ValueForPreId()
        {
            if (this.IsAssignmentOperator(_currenToken.TokenType))
            {
                var assignmentType = _currenToken;
                this.SetNextToken();
                var rightOperand = this.Expression();
                var expression = this.GetAssignmentType(assignmentType);
                expression.RightOperand = rightOperand;
                return expression;  
            }
            else if (_currenToken.TokenType == TokenType.ParenthesisLeft)
            {
                return this.CallFunction();
            }
            return null;
        }

        private CallFunctionStatementNode CallFunctionStatement(IdNode id)
        {
            if (_currenToken.TokenType != TokenType.ParenthesisLeft)
                throw new ParserException("A ( was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var parameters = this.ListOfExpressions();

            if (_currenToken.TokenType != TokenType.ParenthesisRight)
                throw new ParserException("A ) was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
            this.SetNextToken();

            if (_currenToken.TokenType != TokenType.EndOfSentence)
                throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            return new CallFunctionStatementNode { Id = id, Parameters = parameters, CurrentToken = _currenToken };
        }

        private StatementNode SpecialDeclaration()
        {
            if (!this.IsADataType(_currenToken.TokenType))
                throw new ParserException("A Data Types was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            var generalDeclaration = this.GeneralDeclaration();
            return this.TypeOfDeclarationForFunction(generalDeclaration);
        }

        private StatementNode TypeOfDeclarationForFunction(GeneralDeclarationNode generalDeclaration)
        {
            if (_currenToken.TokenType == TokenType.Assignment)
            {
                this.ValueForId();
                return new IdStatementNode { CurrentToken = _currenToken};
                //this.MultiDeclaration();
            }
            else if (_currenToken.TokenType == TokenType.SquareBracketLeft)
            {
                return this.IsArrayDeclaration(generalDeclaration);
                //this.MultiDeclaration();
            }
            else if (_currenToken.TokenType == TokenType.ParenthesisLeft)
            {
                return this.IsFunctionDeclaration(generalDeclaration);
            }
            else if (_currenToken.TokenType == TokenType.EndOfSentence)
            {
                this.SetNextToken();
                return generalDeclaration;
            }
            else
            {
                throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
            }
        }

        private GeneralDeclarationNode GeneralDeclaration()
        {
            if (this.IsADataType(_currenToken.TokenType))
            {
                var type = _currenToken;
                this.SetNextToken();
                var pointers = this.IsPointer();

                var id = _currenToken;
                if (_currenToken.TokenType != TokenType.Id)
                    throw new ParserException("An id was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
                this.SetNextToken();
                return new GeneralDeclarationNode { DataType = type.Lexema, Id = new IdNode { DataType = type.Lexema, Name = id.Lexema, Pointers = pointers }, Pointers = pointers, CurrentToken = _currenToken };
            }
            return null;
        }

        private StatementNode Declaration()
        {
            var generalDeclaration = this.GeneralDeclaration();
            return this.TypeOfDeclaration(generalDeclaration);
        }

        private StatementNode TypeOfDeclaration(GeneralDeclarationNode generalDeclaration)
        {
            if (_currenToken.TokenType == TokenType.SquareBracketLeft)
            {
                var array = this.IsArrayDeclaration(generalDeclaration);
                return this.MultiDeclaration(array, generalDeclaration.DataType);
            }
            else if (_currenToken.TokenType == TokenType.ParenthesisLeft)
            {
                return this.IsFunctionDeclaration(generalDeclaration);
            }
            else if(_currenToken.TokenType == TokenType.EndOfSentence)
            {
                this.SetNextToken();
                return generalDeclaration;
            }
            else 
            {
                var initialitation = this.ValueForId();
                var assignmetn = new AssignmentStatementNode { LeftOperand = generalDeclaration.Id, RightOperand = initialitation.First(), CurrentToken = _currenToken};
                var multideclaration = this.MultiDeclaration(assignmetn, generalDeclaration.DataType);
                if (_currenToken.TokenType != TokenType.EndOfSentence)
                {
                    throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
                }
                this.SetNextToken();
                return multideclaration;
            }
        }

        private StatementNode MultiDeclaration(StatementNode declaration, string dataType)
        {
            if (_currenToken.TokenType == TokenType.Comma)
            {
                return this.OptionalId(declaration, dataType);
            } 
            else
            {
                return declaration;
            }
        }

        private StatementNode IsArrayDeclaration(GeneralDeclarationNode generalDeclaration)
        {
            var array = this.ArrayDeclaration(generalDeclaration);

            if (_currenToken.TokenType != TokenType.EndOfSentence)
                throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            return array;
        }

        private StatementNode ArrayDeclaration(GeneralDeclarationNode generalDeclaration)
        {
            if (_currenToken.TokenType != TokenType.SquareBracketLeft)
                throw new ParserException("A [ was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var size = _currenToken;
            var mySize = this.SizeForArray();

            if (_currenToken.TokenType != TokenType.SquareBracketRight)
                throw new ParserException("A ] was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var bidiArray = this.BidArray();
            var initialized = this.OptionalInitArray();

            if (mySize == null && initialized == null)
                throw new ParserException("ArrayType size missing in row " + size.Row + " column " + size.Column + ".");

            var array = new IdArrayNode { GeneralDeclaration = generalDeclaration, Unidimensional = new ArrayAccessorNode { MySize = mySize }, Bidimensional = bidiArray, Values = initialized, CurrentToken = _currenToken };
            if (initialized != null)
            {
                var newAssignment = new AssignmentStatementNode { Array = array, ArrayInitializer = initialized, CurrentToken = _currenToken };
                return this.MultiDeclaration(newAssignment, generalDeclaration.DataType);
            }

            return this.MultiDeclaration(array, generalDeclaration.DataType);
        }

        private ArrayAccessorNode BidArray()
        {
            if (_currenToken.TokenType == TokenType.SquareBracketLeft)
            {
                this.SetNextToken();
                var mySize = this.SizeForArray();

                if (_currenToken.TokenType != TokenType.SquareBracketRight)
                    throw new ParserException("A ] was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                this.SetNextToken();
                return new ArrayAccessorNode { MySize = mySize, CurrentToken = _currenToken };
            }
            else
            {
                return null;
            }
        }

        private List<ExpressionNode> OptionalInitArray()
        {
            if (_currenToken.TokenType == TokenType.Assignment)
            {
                this.SetNextToken();
                if (_currenToken.TokenType != TokenType.CurlyBraceLeft)
                    throw new ParserException("A { was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                this.SetNextToken();
                var valueList = this.ListOfExpressions();

                if (_currenToken.TokenType != TokenType.CurlyBraceRight)
                    throw new ParserException("A } was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
                this.SetNextToken();

                return valueList;
            }
            return null;
        }

        private FunctionNode IsFunctionDeclaration(GeneralDeclarationNode generalDeclaration)
        {
            if(_currenToken.TokenType != TokenType.ParenthesisLeft)
                throw new ParserException("A ( was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var parameters = this.ParameterList();
            
            if (_currenToken.TokenType != TokenType.ParenthesisRight)
                throw new ParserException("A ) was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
            this.SetNextToken();
            var function = new FunctionNode { DataType = generalDeclaration.DataType, Id = generalDeclaration.Id, Parameters = parameters, CurrentToken = _currenToken };
            return this.OptionalFunctionDeclaration(function);
        }

        private FunctionNode OptionalFunctionDeclaration(FunctionNode function)
        {
            if (_currenToken.TokenType == TokenType.EndOfSentence)
            {
                this.SetNextToken();
                return function;
            }
            else if (_currenToken.TokenType == TokenType.CurlyBraceLeft)
            {
                this.SetNextToken();
                var statements = this.ListOfSpecialSentences();

                if (_currenToken.TokenType != TokenType.CurlyBraceRight)
                    throw new ParserException("A } was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
                this.SetNextToken();
                function.StatementsBlock = statements;
                return function;
            }
            else
            {
                throw new ParserException("A ; or a { was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
            }
        }

        private List<ExpressionNode> IsPointer()
        {
            if (_currenToken.TokenType == TokenType.ArithmeticMultiplication)
            {
                var pointer = _currenToken;
                this.SetNextToken();
                var pointerList = this.IsPointer();
                pointerList.Insert(0, new MultiplicationNode { CurrentToken = _currenToken});
                return pointerList;
            }
            else
            {
                return new List<ExpressionNode>();
            }
        }

        private ExpressionNode SizeForArray()
        {
            if (this.IsSizeForArray(_currenToken.TokenType))
            {
                var actualToken = _currenToken;
                this.SetNextToken();
                return this.GetSizeForArrayType(actualToken);
            }
            else
            {
                return null;
            }
        }

        private void SizeBidArray()
        {
            if (!this.IsSizeBidArray(_currenToken.TokenType))
                throw new ParserException("A Number was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
            this.SetNextToken();
        }

        private StatementNode OptionalId(StatementNode declaration, string dataType)
        {
            if (_currenToken.TokenType == TokenType.Comma)
            {
                var token = _currenToken;
                this.SetNextToken();
                var idList = this.ListOfId(dataType);
                return new CommaStatementNode { LeftOperand = idList, ValueO = token.Lexema, RightOperand = declaration, CurrentToken = _currenToken };
            }
            else
            {
                return declaration;
            }
        }

        private StatementNode ListOfId(string dataType)
        {
            var pointers = this.IsPointer();
            if (_currenToken.TokenType != TokenType.Id)
                throw new ParserException("An LeftOperand was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            var id = _currenToken;
            this.SetNextToken();
            var isArray = this.BidArray();

            if (isArray == null)
            {
                var declaration = new IdStatementNode { Name = id.Lexema, Pointers = pointers, CurrentToken = _currenToken };
                return this.OtherIdOrValue(declaration, dataType);
            }
            else
            {
                var bidArray = this.BidArray();
                var initialized = this.ValueForId();
                var generalDeclaration = new GeneralDeclarationNode { Id = new IdNode { Name = id.Lexema, CurrentToken = _currenToken }, Pointers = pointers, DataType = dataType, CurrentToken = _currenToken };
                var declaration = new IdArrayNode { GeneralDeclaration = generalDeclaration, Unidimensional = isArray, Bidimensional = bidArray, Values = initialized, CurrentToken = _currenToken };
                return this.OtherIdOrValue(declaration, dataType);
            }
            
        }

        private StatementNode OtherIdOrValue(StatementNode declaration, string dataType)
        {
            this.ValueForId();
            return this.OptionalId(declaration, dataType);
        }

        private List<ExpressionNode> ValueForId()
        {
            if (_currenToken.TokenType == TokenType.Assignment)
            {
                this.SetNextToken();
                if(_currenToken.TokenType == TokenType.CurlyBraceLeft)
                {
                    this.SetNextToken();
                    var valueList = this.ListOfExpressions();
                    if (_currenToken.TokenType != TokenType.CurlyBraceRight)
                        throw new ParserException("A } was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                    this.SetNextToken();
                    return valueList;
                }
                else
                {
                    var valueList = new List<ExpressionNode>();
                    var expression = this.Expression();
                    valueList.Add(expression);
                    return valueList;
                }
            }
            else
            {
                return null;
            }
        }

        private List<StatementNode> ParameterList()
        {
            if (this.IsADataType(_currenToken.TokenType))
            {
                var dataType = _currenToken;
                this.SetNextToken();
                var parameters = new List<StatementNode>();
                var parameter = this.ChooseIdType(dataType);
                parameters.Insert(0, parameter);
                return this.OptionalListOfParams(parameters);
            } 
            else
            {
                return null;
            }
        }

        private List<StatementNode> OptionalListOfParams(List<StatementNode> parameters)
        {
            if (_currenToken.TokenType == TokenType.Comma)
            {
                _currenToken = _lexicon.GetNextToken();
                if (!this.IsADataType(_currenToken.TokenType))
                    throw new ParserException("A Data Types was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                var dataType = _currenToken;
                this.SetNextToken();
                var parameter = this.ChooseIdType(dataType);
                parameters.Add(parameter);
                return this.OptionalListOfParams(parameters);
            }
            else
            {
                return parameters;
            }
        }

        private StatementNode ChooseIdType(Token token)
        {
            if (_currenToken.TokenType == TokenType.BinaryAnd)
            {
                var actualToken = _currenToken;
                this.SetNextToken();
                if (_currenToken.TokenType != TokenType.Id)
                    throw new ParserException("An LeftOperand was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                var id = _currenToken;
                this.SetNextToken();
                var array = this.OptionalArray(id, null, token.Lexema);
                return new UnaryAndStatementNode { Operand = array, CurrentToken = _currenToken };
            }
            else if (_currenToken.TokenType == TokenType.ArithmeticMultiplication)
            {
                var pointer = this.IsPointer();

                if (_currenToken.TokenType != TokenType.Id)
                    throw new ParserException("An LeftOperand was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                var id = _currenToken;
                this.SetNextToken();
                return this.OptionalArray(id, pointer, token.Lexema);
            }
            else if (_currenToken.TokenType == TokenType.Id)
            {
                var id = _currenToken;
                this.SetNextToken();
                return this.OptionalArray(id, null, token.Lexema);
            }
            else
            {
                return null;
            }
        }

        private StatementNode OptionalArray(Token id, List<ExpressionNode> pointers, string type)
        {
            if (_currenToken.TokenType == TokenType.SquareBracketLeft)
            {
                this.SetNextToken();
                if(_currenToken.TokenType != TokenType.Number)
                    throw new ParserException("A Number was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                var mySize = this.SizeForArray();
                this.SetNextToken();
                if (_currenToken.TokenType != TokenType.SquareBracketRight)
                    throw new ParserException("A ] was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
                
                var bidArray = this.BidArray();
                var identifier = new IdNode { DataType = type, Name = id.Lexema };
                return new IdArrayNode
                {
                    GeneralDeclaration = new GeneralDeclarationNode { Id = identifier, Pointers = pointers, DataType = type },
                    Unidimensional = new ArrayAccessorNode { MySize = mySize },
                    Bidimensional = bidArray
                };
            }

            return new IdStatementNode { DataType = type, Name = id.Lexema, Pointers = pointers,  };
        }

        private ExpressionNode Expression()
        {
            return this.RelationalExpression();
        }

        private ExpressionNode RelationalExpression()
        {
            var expressionAdition = this.ExpressionAdition();
            return this.RelationalExpressionP(expressionAdition);
        }

        private ExpressionNode RelationalExpressionP(ExpressionNode expressionAddition)
        {
            if (this.IsRelationalOperator(_currenToken.TokenType))
            {
                var actualToken = _currenToken;
                this.SetNextToken();
                BinaryOperatorNode binaryOperator = this.GetRelationalType(actualToken);
                var rightExpression = this.ExpressionAdition();
                binaryOperator.RightOperand = this.RelationalExpressionP(rightExpression);
                binaryOperator.ValueO = actualToken.Lexema;
                binaryOperator.LeftOperand = expressionAddition;
                return binaryOperator;
            }
            else
            {
                return expressionAddition;
            }
        }

        private ExpressionNode ExpressionAdition()
        {
            var expressionMul = this.ExpressionMul();
            return this.ExpressionAditionP(expressionMul);
        }

        private ExpressionNode ExpressionAditionP(ExpressionNode expressionMul)
        {
            if (this.IsAnAdditiveOperator(_currenToken.TokenType))
            {
                var actualToken = _currenToken;
                this.SetNextToken();
                var rightExpression = this.GetAdditiveType(actualToken);
                var expression = this.ExpressionMul();
                rightExpression.LeftOperand = expressionMul;
                rightExpression.ValueO = actualToken.Lexema;
                rightExpression.RightOperand = this.ExpressionAditionP(expression);
                return rightExpression;
            }
            else
            {
                return expressionMul;
            }
        }

        private ExpressionNode ExpressionMul()
        {
            var expression = this.ExpressionUnary();
            return this.ExpressionMulP(expression);
        }

        private ExpressionNode ExpressionMulP(ExpressionNode expression)
        {
            if (this.IsAMultiplicativeOperator(_currenToken.TokenType))
            {
                var actualToken = _currenToken;
                var rightExpression = this.GetMultiplicativeType(actualToken);
                this.SetNextToken();
                var leftExpression =  this.ExpressionUnary();
                rightExpression.LeftOperand = expression;
                rightExpression.ValueO = actualToken.Lexema;
                rightExpression.RightOperand = this.ExpressionMulP(leftExpression);
                return rightExpression;
            }
            else
            {
                return expression;
            }
        }

        private ExpressionNode ExpressionUnary()
        {
            if (this.IsAnUnaryOperator(_currenToken.TokenType))
            {
                var unaryOperator = this.GetUnaryType(_currenToken);
                this.SetNextToken();
                unaryOperator.Operand = this.Factor();
                return unaryOperator;
            }
            else
            {
                return this.Factor();
            }
        }

        private ExpressionNode Factor()
        {
            if (_currenToken.TokenType == TokenType.Id)
            {
                var currentToken = _currenToken;
                this.SetNextToken();
                if (this.IsDecrementOrIncrement(_currenToken.TokenType))
                {
                    this.SetNextToken();
                    return this.GetPosIncrementOrDecrementNode(currentToken.Lexema);
                }
                return this.FactorFunctionOrArray(currentToken);
            }
            else if (_currenToken.TokenType == TokenType.Number || _currenToken.TokenType == TokenType.NumberFloat || _currenToken.TokenType == TokenType.NumberOctal ||
                _currenToken.TokenType == TokenType.NumberHexadecimal || _currenToken.TokenType == TokenType.LiteralString || _currenToken.TokenType == TokenType.KeywordTrue || 
                _currenToken.TokenType == TokenType.KeywordFalse || _currenToken.TokenType == TokenType.LiteralDate || _currenToken.TokenType == TokenType.LiteralChar)
            {
                var number = _currenToken;
                this.SetNextToken();
                return this.GetLiteralType(number);
            }
            else if (_currenToken.TokenType == TokenType.ParenthesisLeft)
            {
                this.SetNextToken();
                var expressionValue = Expression();

                if (_currenToken.TokenType != TokenType.ParenthesisRight)
                    throw new Exception("A ) was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                this.SetNextToken();
                return expressionValue;
            }
            else
            {
                return null;
            }
        }

        private ExpressionNode FactorFunctionOrArray(Token token)
        {
            if (_currenToken.TokenType == TokenType.ParenthesisLeft)
            {
                return this.CallFunction();
            }
            else if (_currenToken.TokenType == TokenType.SquareBracketLeft || _currenToken.TokenType == TokenType.Point || _currenToken.TokenType == TokenType.Pointer)
            {
                var accessors = new List<AccessorNode>();
                return this.IndexOrArrowAccess(token.Lexema, accessors);
            }
            else
            {
                return new IdNode { Name = token.Lexema };
            }
        }

        private ExpressionNode IndexOrArrowAccess(string id, List<AccessorNode> accessors)
        {
            if (_currenToken.TokenType == TokenType.SquareBracketLeft)
            {
                this.SetNextToken();
                var size = this.Expression();

                if (_currenToken.TokenType != TokenType.SquareBracketRight)
                    throw new ParserException("A ] was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                this.SetNextToken();
                accessors.Add(new ArrayAccessorNode { MySize = size });
                this.IndexOrArrowAccess(id, accessors);
                return new IdNode { Name = id, Accessors = accessors };
            }
            else if (this.IsArrowOrDot(_currenToken.TokenType))
            {
                var accessorType = _currenToken;
                this.SetNextToken();
                if (_currenToken.TokenType != TokenType.Id)
                    throw new ParserException("An LeftOperand was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                var identifier = _currenToken;
                this.SetNextToken();

                if (accessorType.TokenType == TokenType.Pointer)
                {
                    var pointer = new PointerNode { Id = new IdNode { Name = identifier.Lexema } };
                    accessors.Add(pointer);
                    this.IndexOrArrowAccess(identifier.Lexema, pointer.Id.Accessors);
                    return new IdNode { Name = id, Accessors = accessors };
                }
                else
                {
                    var point = new PointNode { Id = new IdNode { Name = identifier.Lexema } };
                    accessors.Add(point);
                    this.IndexOrArrowAccess(identifier.Lexema, point.Id.Accessors);
                    return new IdNode { Name = id, Accessors = accessors };
                }
            }
            else
            {
                return new IdNode { Accessors = accessors };
            }
        }

        private CallFunctionNode CallFunction()
        {
            if (_currenToken.TokenType != TokenType.ParenthesisLeft)
                throw new ParserException("A ( was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var parameters = this.ListOfExpressions();

            if (_currenToken.TokenType != TokenType.ParenthesisRight)
                throw new ParserException("A ) was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
            this.SetNextToken();
            return new CallFunctionNode { Parameters = parameters };
        }

        private List<ExpressionNode> ListOfExpressions()
        {
            var expression = this.Expression();
            if (expression == null)
                return null;
            var otherExpression = this.OptionalListOfExpressions();
            otherExpression.Insert(0, expression);
            return otherExpression;
        }

        private List<ExpressionNode> OptionalListOfExpressions()
        {
            if (_currenToken.TokenType == TokenType.Comma)
            {
                this.SetNextToken();
                return this.ListOfExpressions();
            }
            else
            {
                return new List<ExpressionNode>();
            }
        }

        private IfNode If()
        {
            if(_currenToken.TokenType != TokenType.KeywordIf)
                throw new ParserException("An If was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.ParenthesisLeft)
                throw new ParserException("A ( was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var condition = this.Expression();

            if (_currenToken.TokenType != TokenType.ParenthesisRight)
                throw new ParserException("A ) was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var statements = this.BlockForIf();
            var elseStatements = this.Else();

            return new IfNode { BooleanCondition = condition, IfBlock = statements, ElseBlock = elseStatements };
        }

        private List<StatementNode> BlockForIf()
        {
            if (_currenToken.TokenType == TokenType.CurlyBraceLeft)
            {
                this.SetNextToken();
                var statements = this.ListOfSentences();

                if (_currenToken.TokenType != TokenType.CurlyBraceRight)
                    throw new ParserException("A } was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                this.SetNextToken();
                return statements;
            }
            else
            {
                var statements = new List<StatementNode>();
                var statement  = this.Sentence();
                statements.Insert(0, statement);
                return statements;
            }
        }

        private List<StatementNode> BlockForLoop()
        {
            if (_currenToken.TokenType != TokenType.CurlyBraceLeft)
                throw new ParserException("A { was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var loopBlock = this.ListOfSentences();

            if (_currenToken.TokenType != TokenType.CurlyBraceRight)
                throw new ParserException("A } was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            return loopBlock;
        }

        private List<StatementNode> Else()
        {
            if (_currenToken.TokenType == TokenType.KeywordElse)
            {
                this.SetNextToken();
                return this.BlockForIf();
            }
            else
            {
                return null;
            }
        }

        private WhileNode While()
        {
            if (_currenToken.TokenType != TokenType.KeywordWhile)
                throw new ParserException("A While was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.ParenthesisLeft)
                throw new ParserException("A ( was expecting.");

            this.SetNextToken();
            var condition = this.Expression();

            if (_currenToken.TokenType != TokenType.ParenthesisRight)
                throw new ParserException("A ) was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var loopBlock = this.BlockForLoop();
            return new WhileNode { Condition = condition, LoopBlock = loopBlock };
        }

        private DoWhileNode Do()
        {
            if (_currenToken.TokenType != TokenType.KeywordDo)
                throw new ParserException("A Do was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var blockLoop = this.BlockForLoop();

            if (_currenToken.TokenType != TokenType.KeywordWhile)
                throw new ParserException("A While was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.ParenthesisLeft)
                throw new ParserException("A ( was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var condition = this.Expression();

            if (_currenToken.TokenType != TokenType.ParenthesisRight)
                throw new ParserException("A ) was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.EndOfSentence)
                throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            return new DoWhileNode { LoopBlock = blockLoop, Condition = condition };
        }

        private ForGeneralNode ForLoop()
        {
            if (_currenToken.TokenType != TokenType.KeywordFor)
                throw new ParserException("A For was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.ParenthesisLeft)
                throw new ParserException("A ( was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            return this.ForOrForeach();
        }

        private ForGeneralNode ForOrForeach()
        {
            if(this.IsADataType(_currenToken.TokenType))
            {
                var dataType = _currenToken.Lexema;
                this.SetNextToken();
                if (_currenToken.TokenType != TokenType.Id)
                    throw new ParserException("An LeftOperand was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                var variable = new IdNode{ Name = _currenToken.Lexema };
                this.SetNextToken();
                if (_currenToken.TokenType != TokenType.Colon)
                    throw new ParserException("A : was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                this.SetNextToken();
                if (_currenToken.TokenType != TokenType.Id)
                    throw new ParserException("An LeftOperand was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                var listLoop = new IdNode { Name = _currenToken.Lexema };
                this.SetNextToken();
                if (_currenToken.TokenType != TokenType.ParenthesisRight)
                    throw new ParserException("A ) was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                this.SetNextToken();
                var loopBlock = this.BlockForLoop();
                return new ForEachNode { DataType = dataType, Variable = variable, ListLoop = listLoop, LoopBlock = loopBlock };
            }
            else
            {
                var initialExpression = this.Expression();
                if (_currenToken.TokenType != TokenType.EndOfSentence)
                    throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                this.SetNextToken();
                var conditionalExpression = this.Expression();

                this.SetNextToken();
                var incrementExpression = this.Expression();

                if (_currenToken.TokenType != TokenType.ParenthesisRight)
                    throw new ParserException("A ) was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                this.SetNextToken();
                var loopBlock = this.BlockForLoop();
                return new ForNode { InitialExpression = initialExpression, ConditionalExpression = conditionalExpression, IncrementExpression = incrementExpression, LoopBlock = loopBlock };
            }
        }

        private SwitchNode Switch()
        {
            if (_currenToken.TokenType != TokenType.KeywordSwitch)
                throw new ParserException("A Switch was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.ParenthesisLeft)
                throw new ParserException("A ( was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var condition = this.Expression();

            if (_currenToken.TokenType != TokenType.ParenthesisRight)
                throw new ParserException("A ) was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.CurlyBraceLeft)
                throw new ParserException("A { was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var casesList = this.ListOfCase();

            var switchNode = new SwitchNode{ Condition = condition, CaseList = casesList };
            if (_currenToken.TokenType == TokenType.KeywordDefault)
            {
                var defaultCase = this.DefaultCase();
                switchNode.Default = defaultCase;
            }

            if (_currenToken.TokenType != TokenType.CurlyBraceRight)
                throw new ParserException("A } was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            return switchNode;
        }

        private List<CaseNode> ListOfCase()
        {
            if (_currenToken.TokenType == TokenType.KeywordCase)
            {
                var newCase = this.Case();
                var casesList = this.ListOfCase();
                casesList.Insert(0, newCase);
                return casesList;
            }
            else
            {
                return new List<CaseNode>();
            }
        }

        private CaseNode Case()
        {
            if(_currenToken.TokenType != TokenType.KeywordCase)
                throw new ParserException("A Case was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var condition = this.Expression();

            if(_currenToken.TokenType != TokenType.Colon)
                throw new ParserException("A : was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var statements = this.ListOfSpecialSentences();
            var breakNode = this.Break();
            return new CaseNode { Condition = condition, StatementsBlock = statements, Break = breakNode };
        }

        private DefaultNode DefaultCase()
        {
            if (_currenToken.TokenType != TokenType.KeywordDefault)
                throw new ParserException("A Default was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.Colon)
                throw new ParserException("A : was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var statements = this.ListOfSpecialSentences();
            return new DefaultNode { StatementsBlock = statements };
        }

        private BreakNode Break()
        {
            if(_currenToken.TokenType == TokenType.KeywordBreak)
            {
                this.SetNextToken();
                if (_currenToken.TokenType != TokenType.EndOfSentence)
                    throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
                this.SetNextToken();
                return new BreakNode();
            }
            else
            {
                return null;
            }
        }

        private StructNode Struct()
        {
            if (_currenToken.TokenType != TokenType.KeywordStruct)
                throw new ParserException("A StructType was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if(_currenToken.TokenType != TokenType.Id)
                throw new ParserException("An LeftOperand was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            var dataType = _currenToken.Lexema;
            this.SetNextToken();
            var structNode = new StructNode();
            structNode = this.StructDeclarationOrInitialization(structNode);
           
            if (_currenToken.TokenType != TokenType.EndOfSentence)
                throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            return structNode;
        }

        private StructNode StructDeclarationOrInitialization(StructNode structNode)
        {
            if (_currenToken.TokenType == TokenType.CurlyBraceLeft)
            {
                this.SetNextToken();
                var members = this.MemberList();

                if (_currenToken.TokenType != TokenType.CurlyBraceRight)
                    throw new ParserException("A } was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
                
                this.SetNextToken();
                var pointers = this.IsPointer();
                if(_currenToken.TokenType == TokenType.Id)
                {
                    this.SetNextToken();
                    structNode.Varibles = new List<StatementNode>();
                    return this.ListOfStructDeclarations(structNode);
                }
                structNode.Members = members;
                return structNode;
            } 
            else
            {
                var pointers = this.IsPointer();
                if (_currenToken.TokenType != TokenType.Id)
                    throw new ParserException("An LeftOperand was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                var id = new IdNode { Name = _currenToken.Lexema };
                this.SetNextToken();
                if (_currenToken.TokenType == TokenType.SquareBracketLeft)
                {
                    
                    var generalDeclaration = new GeneralDeclarationNode { Id = id, Pointers = pointers };
                    var arrayId = new IdArrayNode { GeneralDeclaration = generalDeclaration , };
                    var array = this.ArrayDeclaration(generalDeclaration);
                }
                return this.ListOfStructDeclarations(structNode);
            }
        }

        private StructNode ListOfStructDeclarations(StructNode structNode)
        {
            if(_currenToken.TokenType == TokenType.Comma)
            {
                var comma = _currenToken;
                this.SetNextToken();
                var pointers = this.IsPointer();

                if (_currenToken.TokenType != TokenType.Id)
                    throw new ParserException("An LeftOperand was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                var generalDeclaration = new IdNode { Name = _currenToken.Lexema, Pointers = pointers };
                var newStruct = new StructNode { Members = structNode.Members };
                this.SetNextToken();
                if (_currenToken.TokenType == TokenType.SquareBracketLeft)
                {
                    //this.ArrayDeclaration();
                }
                return this.ListOfStructDeclarations(structNode);
            }
            else
            {
                return structNode;
            }
        }

        private List<StatementNode> MemberList()
        {
            return this.DeclarationOfStruct();
        }

        private List<StatementNode> DeclarationOfStruct()
        {
            if (!this.IsADataType(_currenToken.TokenType))
            {
                throw new ParserException("A Data Types was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
            }
            else
            {
                var generalDeclaration = this.GeneralDeclaration();
                var uniArray = this.ArrayIdentifier();
                var bidArray = this.BidArray();

                if(uniArray == null)
                {
                    var declaration = this.MultiDeclaration(generalDeclaration, generalDeclaration.DataType);
                    if (_currenToken.TokenType != TokenType.EndOfSentence)
                        throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");
                    
                    this.SetNextToken();
                    var declarationList = this.OptionalMember();
                    declarationList.Insert(0, declaration);
                    return declarationList;
                }
                else
                {
                    var declaration = new IdArrayNode { GeneralDeclaration = generalDeclaration, Unidimensional = uniArray, Bidimensional = bidArray };
                    var secondDeclaration = this.MultiDeclaration(declaration, generalDeclaration.DataType);

                    if (_currenToken.TokenType != TokenType.EndOfSentence)
                        throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                    this.SetNextToken();
                    var declarationList = this.OptionalMember();
                    declarationList.Insert(0, secondDeclaration);
                    return declarationList;
                }
            }
        }

        private List<StatementNode> OptionalMember()
        {
            if (this.IsADataType(_currenToken.TokenType))
            {
                return this.DeclarationOfStruct();
            }
            else
            {
                return new List<StatementNode>();
            }
        }

        private ArrayAccessorNode ArrayIdentifier()
        {
            if(_currenToken.TokenType == TokenType.SquareBracketLeft)
            {
                this.SetNextToken();
                var mySize = this.SizeForArray();

                if (_currenToken.TokenType != TokenType.SquareBracketRight)
                    throw new ParserException("A ] was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                this.SetNextToken();
                return new ArrayAccessorNode { MySize = mySize };
            }
            else
            {
                return null;
            }
        }

        private ConstNode Const()
        {
            if (_currenToken.TokenType != TokenType.KeywordConst)
                throw new ParserException("A Const was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var dataType = _currenToken.Lexema;
            if(!this.IsADataType(_currenToken.TokenType))
                throw new ParserException("A Data Types was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var id = new IdNode { Name = _currenToken.Lexema, DataType = dataType };
            if (_currenToken.TokenType != TokenType.Id)
                throw new ParserException("An LeftOperand was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.Assignment)
                throw new ParserException("An = was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var variable = this.Expression();

            if (_currenToken.TokenType != TokenType.EndOfSentence)
                throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            return new ConstNode { Id = id, Initialitation = variable };
        }

        private EnumNode Enum()
        {
            if (_currenToken.TokenType != TokenType.KeywordEnum)
                throw new ParserException("An Enum was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var id = _currenToken;
            if(_currenToken.TokenType != TokenType.Id)
                throw new ParserException("An LeftOperand was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.CurlyBraceLeft)
                throw new ParserException("A { was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            var items = this.EnumeratorList();

            if (_currenToken.TokenType != TokenType.CurlyBraceRight)
                throw new ParserException("A } was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            if (_currenToken.TokenType != TokenType.EndOfSentence)
                throw new ParserException("A ; was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            this.SetNextToken();
            return new EnumNode { Id = new IdNode { Name = id.Lexema }, Items = items };
        }

        private List<ExpressionNode> EnumeratorList()
        {
            if (_currenToken.TokenType == TokenType.Id)
            {
                var item = this.EnumItem();
                var enumList = this.OptionalEnumItem();
                enumList.Insert(0, item);
                return enumList;
            } 
            else
            {
                return new List<ExpressionNode>();
            }
        }

        private List<ExpressionNode> OptionalEnumItem()
        {
            if (_currenToken.TokenType == TokenType.Comma)
            {
                this.SetNextToken();
            }
            return this.EnumeratorList();
        }

        private ExpressionNode EnumItem()
        {
            if(_currenToken.TokenType != TokenType.Id)
                throw new ParserException("An LeftOperand was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

            var id = _currenToken;
            this.SetNextToken();
            return this.OptionalIndexPosition(id);
        }

        private ExpressionNode OptionalIndexPosition(Token currentToken)
        {
            var idNode = new IdNode { Name = currentToken.Lexema };
            if (_currenToken.TokenType == TokenType.Assignment)
            {
                this.SetNextToken();
                if (_currenToken.TokenType != TokenType.Number)
                    throw new ParserException("A Number was expecting in row " + _currenToken.Row + " column " + _currenToken.Column + ".");

                var expression = _currenToken;
                this.SetNextToken();
                return new AssignmentNode { LeftOperand = idNode, RightOperand = new NumberLiteralNode { Value = Convert.ToInt32(expression.Lexema) } };
            }
            else
            {
                return idNode;
            }
        }

        private void SetNextToken()
        {
            _currenToken = _lexicon.GetNextToken();
            if (_currenToken.TokenType == TokenType.Html)
            {
                HtmlStack.StackInstance.Stack.Add(new StringValue { MyValue = _currenToken.Lexema });
                _currenToken = _lexicon.GetNextToken();
            }
               
        }

        private bool IsADataType(TokenType currentToken)
        {
            if (currentToken == TokenType.KeywordInt || currentToken == TokenType.KeywordFloat || currentToken == TokenType.KeywordChar ||
                currentToken == TokenType.KeywordBool || currentToken == TokenType.KeywordString || currentToken == TokenType.KeywordDate ||
                currentToken == TokenType.KeywordVoid || currentToken == TokenType.KeywordDouble || currentToken == TokenType.KeywordLong)
                return true;
            return false;
        }

        private bool IsASentence(TokenType currentToken)
        {
            if (currentToken == TokenType.KeywordIf || currentToken == TokenType.KeywordWhile || currentToken == TokenType.KeywordDo ||
                currentToken == TokenType.KeywordFor || currentToken == TokenType.KeywordSwitch || currentToken == TokenType.KeywordStruct ||
                currentToken == TokenType.KeywordConst || currentToken == TokenType.KeywordBreak || currentToken == TokenType.KeywordContinue ||
                currentToken == TokenType.KeywordEnum || currentToken == TokenType.KeywordPrintf || currentToken == TokenType.KeywordInclude)
                return true;
            return false;
        }

        private bool IsAnSpecialSentence(TokenType currentToken)
        {
            if (currentToken == TokenType.KeywordIf || currentToken == TokenType.KeywordWhile || currentToken == TokenType.KeywordDo ||
                currentToken == TokenType.KeywordFor || currentToken == TokenType.KeywordSwitch || currentToken == TokenType.KeywordConst || 
                currentToken == TokenType.KeywordBreak || currentToken == TokenType.KeywordContinue || currentToken == TokenType.KeywordEnum ||
                currentToken == TokenType.KeywordReturn || currentToken == TokenType.KeywordPrintf)
                return true;
            return false;
        }

        private bool IsAnUnaryOperator(TokenType currentToken)
        {
            if (currentToken == TokenType.BinaryComplement || IsDecrementOrIncrement(currentToken) || currentToken == TokenType.ArithmeticSubstract ||
                currentToken == TokenType.BinaryAnd || currentToken == TokenType.BinaryOr || currentToken == TokenType.BinaryXor)
                return true;
            return false;
        }

        private bool IsAMultiplicativeOperator(TokenType currentToken)
        {
            if (currentToken == TokenType.ArithmeticMultiplication || currentToken == TokenType.ArithmeticDivision || currentToken == TokenType.ArithmeticMod)
                return true;
            return false;
        }

        private bool IsSizeForArray(TokenType currentToken)
        {
            if (currentToken == TokenType.Id || currentToken == TokenType.Number || currentToken == TokenType.NumberOctal || currentToken == TokenType.NumberHexadecimal)
                return true;
            return false;
        }

        private bool IsArrowOrDot(TokenType currentToken)
        {
            if (currentToken == TokenType.Point || currentToken == TokenType.Pointer)
                return true;
            return false;
        }

        private bool IsAnAdditiveOperator(TokenType currentToken)
        {
            if (currentToken == TokenType.ArithmeticAdd || currentToken == TokenType.ArithmeticSubstract)
                return true;
            return false;
        }

        private bool IsSizeBidArray(TokenType currentToken)
        {
            if (currentToken == TokenType.Id || currentToken == TokenType.Number || currentToken == TokenType.NumberOctal ||
                currentToken == TokenType.NumberHexadecimal || currentToken == TokenType.NumberFloat)
                return true;
            return false;
        }

        private bool IsDecrementOrIncrement(TokenType currentToken)
        {
            if (currentToken == TokenType.ArithmeticDecrement || currentToken == TokenType.ArithemeticIncrement)
                return true;
            return false;
        }

        private bool IsRelationalOperator(TokenType currentToken)
        {
            if (currentToken == TokenType.RelationalLessThan || currentToken == TokenType.RelationalLessOrEqualThan || currentToken == TokenType.RelationalGreaterThan || 
                currentToken == TokenType.RelationalGreaterOrEqualThan || currentToken == TokenType.LogicalAnd || currentToken == TokenType.LogicalOr || currentToken == TokenType.LogicalNot ||
                currentToken == TokenType.BinaryRightShift || currentToken == TokenType.BinaryLeftShift || currentToken == TokenType.BinaryAnd || currentToken == TokenType.BinaryOr || currentToken == TokenType.BinaryXor ||
                currentToken == TokenType.BinaryComplement ||  currentToken == TokenType.RelationalEqualTo || currentToken == TokenType.RelationalDifferentThan || currentToken == TokenType.AssignmentAdd || 
                currentToken == TokenType.AssignmentSubstract || currentToken == TokenType.AssignmentMultiplication || currentToken == TokenType.AssignmentDivision || currentToken == TokenType.AssignmentMod ||
                currentToken == TokenType.AssignmentAnd || currentToken == TokenType.AssignmentXor || currentToken == TokenType.AssignmentOr || currentToken == TokenType.Assignment)
                return true;
            return false;
        }

        private bool IsAssignmentOperator(TokenType currentToken)
        {
            if (currentToken == TokenType.Assignment || currentToken == TokenType.AssignmentAdd || currentToken == TokenType.AssignmentSubstract ||
                currentToken == TokenType.AssignmentMultiplication || currentToken == TokenType.AssignmentDivision || currentToken == TokenType.AssignmentMod ||
                currentToken == TokenType.AssignmentLeftShiftBits || currentToken == TokenType.AssignmentRightShiftBits || currentToken == TokenType.AssignmentAnd ||
                currentToken == TokenType.AssignmentXor || currentToken == TokenType.AssignmentOr)
                return true;
            return false;
        }

        private BinaryOperatorNode GetRelationalType(Token token)
        {
            BinaryOperatorNode typeToReturn = null;
            switch (token.TokenType)
            {
                case TokenType.RelationalLessThan:
                    typeToReturn = new LessThanNode();
                    break;
                case TokenType.RelationalLessOrEqualThan:
                    typeToReturn = new LessThanOrEqualNode();
                    break;
                case TokenType.RelationalGreaterThan:
                    typeToReturn = new GreaterThanNode();
                    break;
                case TokenType.RelationalGreaterOrEqualThan:
                    typeToReturn = new GreaterOrEqualThanNode();
                    break;
                case TokenType.LogicalAnd:
                    typeToReturn = new LogicalAndNode();
                    break;
                case TokenType.LogicalOr:
                    typeToReturn = new LogicalOrNode();
                    break;
                case TokenType.LogicalNot:
                    typeToReturn = new LogicalNotNode();
                    break;
                case TokenType.BinaryLeftShift:
                    typeToReturn = new BinaryLeftShiftNode();
                    break;
                case TokenType.BinaryRightShift:
                    typeToReturn = new BinaryRightShiftNode();
                    break;
                case TokenType.BinaryAnd:
                    typeToReturn = new BinaryAndNode();
                    break;
                case TokenType.BinaryOr:
                    typeToReturn = new BinaryOrNode();
                    break;
                case TokenType.BinaryXor:
                    typeToReturn = new BinaryXorNode();
                    break;
                case TokenType.BinaryComplement:
                    typeToReturn = new BinaryComplementNode();
                    break;
                case TokenType.RelationalEqualTo:
                    typeToReturn = new EqualToNode();
                    break;
                case TokenType.RelationalDifferentThan:
                    typeToReturn = new DifferentThanNode();
                    break;
                case TokenType.AssignmentAdd:
                case TokenType.AssignmentSubstract:
                case TokenType.AssignmentMultiplication:
                case TokenType.AssignmentDivision:
                case TokenType.AssignmentMod:
                case TokenType.AssignmentAnd:
                case TokenType.AssignmentOr:
                case TokenType.AssignmentXor:
                case TokenType.Assignment:
                    typeToReturn = this.GetAssignmentType(token);
                    break;
                default:
                    typeToReturn = null;
                    break;
            }
            return typeToReturn;
        }

        private BinaryOperatorNode GetAdditiveType(Token token)
        {
            var assignmentType = token.TokenType;
            if (assignmentType == TokenType.ArithmeticAdd)
                return new AdditionNode();
            else
                return new SubtractionNode();
        }

        private BinaryOperatorNode GetMultiplicativeType(Token token)
        {
            var assignmentType = token.TokenType;
            if (assignmentType == TokenType.ArithmeticMultiplication)
                return new MultiplicationNode();
            else if (assignmentType == TokenType.ArithmeticDivision)
                return new DivisionNode();
            else
                return new ModNode();
        }

        private BinaryOperatorNode GetAssignmentType(Token token)
        {
            var assignmentType = token.TokenType;
            if (assignmentType == TokenType.Assignment)
                return new AssignmentNode();
            if (assignmentType == TokenType.AssignmentAdd)
                return new AdditionAssignmentNode();
            else if (assignmentType == TokenType.AssignmentSubstract)
                return new SubtractionAssignmentNode();
            else if (assignmentType == TokenType.AssignmentMultiplication)
                return new MultiplicationAssignmentNode();
            else if (assignmentType == TokenType.AssignmentDivision)
                return new DivisionAssignmentNode();
            else if (assignmentType == TokenType.AssignmentMod)
                return new ModAssignmentNode();
            else if (assignmentType == TokenType.AssignmentAnd)
                return new BinaryAndAssignmentNode();
            else if (assignmentType == TokenType.AssignmentOr)
                return new BinaryOrAssignmentNode();
            else if (assignmentType == TokenType.AssignmentXor)
                return new BinaryXorAssignmentNode();
                else if (assignmentType == TokenType.AssignmentLeftShiftBits)
                return new BinaryShiftLeftAssignmentNode();
            else if (assignmentType == TokenType.AssignmentRightShiftBits)
                return new BinaryShiftRightAssignmentNode();
            else
                return null;
        }

        private ExpressionNode GetLiteralType(Token currentToken)
        {
            switch(currentToken.TokenType)
            {
                case TokenType.Number:
                    return new NumberLiteralNode { Value = Int32.Parse(currentToken.Lexema) };
                case TokenType.NumberHexadecimal:
                    return new HexadecimalLiteralNode { Value = Convert.ToInt32(currentToken.Lexema, 16) };
                case TokenType.NumberOctal:
                    return new OctalLiteralNode { Value = Convert.ToInt32(currentToken.Lexema, 8) };
                case TokenType.NumberFloat:
                    return new FloatLiteralNode { Value = float.Parse(currentToken.Lexema) };
                case TokenType.LiteralChar:
                    return new CharLiteralNode { Value = Char.Parse(currentToken.Lexema) };
                case TokenType.LiteralString:
                    return new StringLiteralNode { Value = currentToken.Lexema };
                case TokenType.KeywordFalse:
                    return new BooleanLiteralNode { Value = false };
                case TokenType.KeywordTrue:
                    return new BooleanLiteralNode { Value = true };
                case TokenType.LiteralDate:
                    return new DateLiteralNode { Value = DateTime.Parse(currentToken.Lexema) };
                default:
                    return null;
            }
        }

        private ExpressionNode GetPosIncrementOrDecrementNode(string lexema)
        {
            var idOperand = new IdNode { Name = lexema };
            if (_currenToken.TokenType == TokenType.ArithemeticIncrement)
                return new IncrementOperatorNode { Operand = idOperand };
            else
                return new DecrementOperatorNode { Operand = idOperand };
        }

        private UnaryOperatorNode GetUnaryType(Token token)
        {
            var unaryType = token.TokenType;
            if (unaryType == TokenType.ArithmeticSubstract)
                return new NegativeNumberNode();
            else if (unaryType == TokenType.BinaryComplement)
                return new ComplementNode();
            else if (unaryType == TokenType.ArithemeticIncrement)
                return new IncrementOperatorNode();
            else if (unaryType == TokenType.ArithmeticDecrement)
                return new DecrementOperatorNode();
            else if (unaryType == TokenType.BinaryAnd)
                return new UnaryAndNode();
            else if (unaryType == TokenType.BinaryOr)
                return new UnaryOrNode();
            else if (unaryType == TokenType.BinaryXor)
                return new UnaryXorNode();
            else
                return null;
        }

        private ExpressionNode GetSizeForArrayType(Token token)
        {
            switch (token.TokenType)
            {
                case TokenType.Number:
                    return new NumberLiteralNode { Value = Int32.Parse(token.Lexema) };
                case TokenType.NumberHexadecimal:
                    return new HexadecimalLiteralNode { Value = Convert.ToInt32(token.Lexema, 16) };
                case TokenType.NumberOctal:
                    return new OctalLiteralNode { Value = Convert.ToInt32(token.Lexema, 8) };
                case TokenType.NumberFloat:
                    return new FloatLiteralNode { Value = float.Parse(token.Lexema) };
                case TokenType.Id:
                    return new IdNode { Name = token.Lexema };
                default:
                    return null;
            }
        }
    }
}