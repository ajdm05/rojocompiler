﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoJoCompiler.SyntacticTree.Expression.BinaryOperator
{
    public class AdditionNode : BinaryOperatorNode
    {
        public override DataType EvaluateSemantic()
        {
            var leftType = LeftOperand.EvaluateSemantic();
            var rightType = RightOperand.EvaluateSemantic();

            if (leftType is StringType && rightType is CharType)
                return new StringType();

            if((leftType is IntType && rightType is FloatType) || (rightType is IntType && leftType is FloatType))
                return new FloatType();

            if ((leftType is CharType && rightType is CharType) || (leftType is IntType && rightType is DateType))
                return new IntType();

            if((leftType is CharType && rightType is IntType) || (leftType is IntType && rightType is CharType))
                return new CharType();

            if ((leftType is IntType && rightType is BoolType) || (rightType is IntType && leftType is BoolType))
                return new IntType();

            if (leftType is DateType && rightType is IntType)
                return new DateType();

            if(leftType.GetType() == rightType.GetType())
                return leftType;

            throw new SemanticException("Could not add between " + leftType + " and " + rightType + " in row " + CurrentToken.Row + " column " + CurrentToken.Column);
        }

        public override Value Interpret()
        {
            var leftValue = LeftOperand.Interpret();
            var rightValue = RightOperand.Interpret();

            if (leftValue is StringValue && rightValue is CharValue)
                return new StringValue { MyValue = ((StringValue)leftValue).MyValue + ((CharValue)rightValue).MyValue };

            if (leftValue is IntValue && rightValue is FloatValue)
                return new FloatValue { MyValue = ((IntValue)leftValue).MyValue + ((FloatValue)rightValue).MyValue };

            if (leftValue is FloatValue && rightValue is IntValue)
                return new FloatValue { MyValue = ((FloatValue)leftValue).MyValue + ((IntValue)rightValue).MyValue };

            if (leftValue is CharValue && rightValue is CharValue)
                return new IntValue { MyValue = ((CharValue)leftValue).MyValue + ((CharValue)rightValue).MyValue };

            if (leftValue is IntValue && rightValue is DateValue)
                return new IntValue { MyValue = ((DateValue)leftValue).MyValue.AddSeconds(((IntValue)rightValue).MyValue).Second };

            if (leftValue is CharValue && rightValue is IntValue)
                return new IntValue { MyValue = ((CharValue)leftValue).MyValue + ((IntValue)rightValue).MyValue };

            if (leftValue is IntValue && rightValue is CharValue)
                return new IntValue { MyValue = ((IntValue)leftValue).MyValue + ((CharValue)rightValue).MyValue };

            if (leftValue is IntValue && rightValue is BoolValue)
                return new IntValue { MyValue = ((IntValue)leftValue).MyValue + Convert.ToInt32(((BoolValue)rightValue).MyValue) };

            if (leftValue is BoolValue && rightValue is IntValue)
                return new IntValue { MyValue = Convert.ToInt32(((BoolValue)leftValue).MyValue) + ((IntValue)rightValue).MyValue };

            if (leftValue is DateValue && rightValue is IntValue)
                return new DateValue { MyValue = ((DateValue)leftValue).MyValue.AddDays(((IntValue)rightValue).MyValue) };

            if (leftValue is BoolValue && rightValue is BoolValue)
                return new BoolValue { MyValue = ((BoolValue)leftValue).MyValue || ((BoolValue)rightValue).MyValue };

            if (leftValue is DateValue && rightValue is DateValue)
            {
                var a = ((DateValue)leftValue).MyValue.Add(((DateValue)rightValue).MyValue.TimeOfDay);
                return new DateValue { MyValue =  a };
            }
                

            if (leftValue is FloatValue && rightValue is FloatValue)
                return new FloatValue { MyValue = ((FloatValue)leftValue).MyValue + ((FloatValue)rightValue).MyValue };

            if (leftValue is IntValue && rightValue is IntValue)
                return new IntValue { MyValue = ((IntValue)leftValue).MyValue + ((IntValue)rightValue).MyValue };

            if (leftValue is StringValue && rightValue is StringValue)
                return new StringValue { MyValue = ((StringValue)leftValue).MyValue + ((StringValue)rightValue).MyValue };

            return null;
        }
    }
}
