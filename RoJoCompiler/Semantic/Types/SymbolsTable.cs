﻿using RoJoCompiler.Interpretation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Semantic.Types
{
    public class SymbolsTable
    {
        private static SymbolsTable _instance = null;

        private readonly Dictionary<string, DataType> _variables;

        private readonly Dictionary<string, Value> _values;

        private SymbolsTable()
        {
            _variables = new Dictionary<string, DataType>();
            _values = new Dictionary<string, Value>();
        }

        public static SymbolsTable Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new SymbolsTable();
                return _instance;
            }
        }

        public void DeclareVariable(string name, DataType dataType)
        {
            _variables.Add(name, dataType);
            _values.Add(name, dataType.GetDefaultValue());
        }

        public DataType GetVariable(string name)
        {
            return _variables[name];
        }

        public void SetVariableValue(string name, Value value)
        {
            _values[name] = value;
        }

        public Value GetVariableValue(string name)
        {
            return _values[name];
        }

        public bool ExistVariable(string name)
        {
            return _variables.ContainsKey(name);
        }
    }
}
