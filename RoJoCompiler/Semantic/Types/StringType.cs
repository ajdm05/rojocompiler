﻿using RoJoCompiler.Interpretation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Semantic.Types
{
    public class StringType : DataType
    {
        public override Value GetDefaultValue()
        {
            return new StringValue { MyValue = "" };
        }

        public override string ToString()
        {
            return "string";
        }
    }
}
