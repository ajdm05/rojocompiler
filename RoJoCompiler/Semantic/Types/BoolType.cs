﻿using RoJoCompiler.Interpretation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.Semantic.Types
{
    public class BoolType : DataType
    {
        public override Value GetDefaultValue()
        {
            return new BoolValue { MyValue = false };
        }

        public override string ToString()
        {
            return "bool";
        }
    }
}
