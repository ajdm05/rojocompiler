﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Expression.Literal
{
    public class HexadecimalLiteralNode : ExpressionNode
    {
        public int Value { get; set; }

        public override DataType EvaluateSemantic()
        {
            return new HexadecimalLiteral();
        }

        public override Value Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
