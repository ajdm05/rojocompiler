﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Semantic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoJoCompiler.SyntacticTree.Expression.Literal
{
    public class NumberLiteralNode : ExpressionNode
    {
        public int Value { get; set; }

        public override DataType EvaluateSemantic()
        {
            return new IntType();
        }

        public override Value Interpret()
        {
            return new IntValue { MyValue = this.Value };
        }
    }
}
