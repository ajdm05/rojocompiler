﻿using RoJoCompiler.Interpretation;
using RoJoCompiler.Lexicon;
using RoJoCompiler.Semantic.Types;
using RoJoCompiler.Syntactic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoJoCompiler
{
    class Program
    {
        static void Main(string[] args)
        {
            string sourceCode = "";
            //StreamReader file = new StreamReader("C:/Users/ajdm/Desktop/VPeriodo/Compi_I/RoJoCompiler/RoJoCompiler/code.c");
            System.IO.StreamReader file = new StreamReader("C:/Users/ajdm/Desktop/VPeriodo/Compi_I/RoJoCompiler/RoJoCompiler/SourceCodeParser.txt");
            sourceCode = file.ReadToEnd().ToString();
            var lexicon = new Lexicon.Lexicon("<% int a = 7 + 1; printf(a);%>" + '\0');

            //var accessorType = lexicon.GetNextToken();
            //while (accessorType.TokenType != TokenType.EndOfFile)
            //{
            //    Console.WriteLine(accessorType.ToString());
            //    accessorType = lexicon.GetNextToken();
            //}

            Parser parser = new Parser(lexicon);
            try
            {
                var expressions = parser.Parse();
                foreach (var exp in expressions)
                {
                    exp.ValidateSemantic();
                }

                foreach (var exp in expressions)
                {
                    exp.Interpret();
                }

                if (HtmlStack.StackInstance.LastHtml != null)
                    HtmlStack.StackInstance.Stack.Add(HtmlStack.StackInstance.LastHtml);

                var stack = HtmlStack.StackInstance.Stack;
                var x = "";
                foreach(var s in stack)
                {
                    if (s is BoolValue)
                        x += ((BoolValue)s).MyValue;

                    if (s is CharValue)
                        x += ((CharValue)s).MyValue;

                    if (s is DateValue)
                        x += ((DateValue)s).MyValue;

                    if (s is FloatValue)
                        x += ((FloatValue)s).MyValue;

                    if (s is IntValue)
                        x += ((IntValue)s).MyValue;

                    if (s is StringValue)
                        x += ((StringValue)s).MyValue;
                }
                Console.WriteLine(x);
            }
            catch (ParserException ex)
            {
                Console.WriteLine(ex.Message);
            }
            

            Console.ReadKey();
        }
    }
}
