﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class ReturnNode : StatementNode
    {
        public ExpressionNode ValueToReturn { get; set; }

        public override void ValidateSemantic()
        {
            ValueToReturn.EvaluateSemantic();
        }

        public override void Interpret()
        {
            throw new NotImplementedException();
        }
    }
}
