﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoJoCompiler.SyntacticTree.Statement
{
    public class ForNode : ForGeneralNode
    {
        public ExpressionNode InitialExpression { get; set; }

        public ExpressionNode ConditionalExpression { get; set; }

        public ExpressionNode IncrementExpression { get; set; }
    }
}
