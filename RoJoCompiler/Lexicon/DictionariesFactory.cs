﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoJoCompiler.Lexicon
{
    public static class DictionariesFactory
    {
        public static Dictionary<string, TokenType> GetGeneralSymbolsDictionary()
        {
            var symbols = new Dictionary<string, TokenType>();
            symbols.Add(";", TokenType.EndOfSentence);
            symbols.Add(",", TokenType.Comma);
            symbols.Add(":", TokenType.Colon);
            symbols.Add("(", TokenType.ParenthesisLeft);
            symbols.Add(")", TokenType.ParenthesisRight);
            symbols.Add("{", TokenType.CurlyBraceLeft);
            symbols.Add("}", TokenType.CurlyBraceRight);
            symbols.Add("[", TokenType.SquareBracketLeft);
            symbols.Add("]", TokenType.SquareBracketRight);
            symbols.Add("#", TokenType.Sharp);
            symbols.Add(".", TokenType.Point);
            symbols.Add("->", TokenType.Pointer);
            symbols.Add("?", TokenType.QuestionMark);
            return symbols;
        }

        public static Dictionary<string, TokenType> GetArithmeticSymbolsDictionary()
        {
            var operationSymbols = new Dictionary<string, TokenType>();
            operationSymbols.Add("+", TokenType.ArithmeticAdd);
            operationSymbols.Add("-", TokenType.ArithmeticSubstract);
            operationSymbols.Add("*", TokenType.ArithmeticMultiplication);
            operationSymbols.Add("/", TokenType.ArithmeticDivision);
            operationSymbols.Add("%", TokenType.ArithmeticMod);
            operationSymbols.Add("++", TokenType.ArithemeticIncrement);
            operationSymbols.Add("--", TokenType.ArithmeticDecrement);
            return operationSymbols;
        }

        public static Dictionary<string, TokenType> GetAssigmentSymbolsDictionary()
        {
            var assigmentSymbols = new Dictionary<string, TokenType>();
            assigmentSymbols.Add("=", TokenType.Assignment);
            assigmentSymbols.Add("+=", TokenType.AssignmentAdd);
            assigmentSymbols.Add("-=", TokenType.AssignmentSubstract);
            assigmentSymbols.Add("*=", TokenType.AssignmentMultiplication);
            assigmentSymbols.Add("/=", TokenType.AssignmentDivision);
            assigmentSymbols.Add("%=", TokenType.AssignmentMod);
            assigmentSymbols.Add(">>=", TokenType.AssignmentRightShiftBits);
            assigmentSymbols.Add("<<=", TokenType.AssignmentLeftShiftBits);
            assigmentSymbols.Add("&=", TokenType.AssignmentAnd);
            assigmentSymbols.Add("^=", TokenType.AssignmentXor);
            assigmentSymbols.Add("|=", TokenType.AssignmentOr);
            return assigmentSymbols;
        }

        public static Dictionary<string, TokenType> GetRelationalSymbolsDictionary()
        {
            var relationalSymbols = new Dictionary<string, TokenType>();
            relationalSymbols.Add(">", TokenType.RelationalGreaterThan);
            relationalSymbols.Add("<", TokenType.RelationalLessThan);
            relationalSymbols.Add(">=", TokenType.RelationalGreaterOrEqualThan);
            relationalSymbols.Add("<=", TokenType.RelationalLessOrEqualThan);
            relationalSymbols.Add("==", TokenType.RelationalEqualTo);
            relationalSymbols.Add("!=", TokenType.RelationalDifferentThan);
            return relationalSymbols;
        }

        public static Dictionary<string, TokenType> GetLogicalSymbolsDictionary()
        {
            var logicSymbols = new Dictionary<string, TokenType>();
            logicSymbols.Add("&&", TokenType.LogicalAnd);
            logicSymbols.Add("||", TokenType.LogicalOr);
            logicSymbols.Add("!", TokenType.LogicalNot);
            return logicSymbols;
        }

        public static Dictionary<string, TokenType> GetBinarySymbolsDictionary()
        {
            var bitwiseSymbols = new Dictionary<string, TokenType>();
            bitwiseSymbols.Add("&", TokenType.BinaryAnd);
            bitwiseSymbols.Add("|", TokenType.BinaryOr);
            bitwiseSymbols.Add("^", TokenType.BinaryXor);
            bitwiseSymbols.Add("~", TokenType.BinaryComplement);
            bitwiseSymbols.Add("<<", TokenType.BinaryLeftShift);
            bitwiseSymbols.Add(">>", TokenType.BinaryRightShift);
            return bitwiseSymbols;
        }

        public static Dictionary<string, TokenType> GetReservedKeywordsDictionary()
        {
            var keywords = new Dictionary<string, TokenType>();
            keywords.Add("while", TokenType.KeywordWhile);
            keywords.Add("volatile", TokenType.KeywordVolatile);
            keywords.Add("void", TokenType.KeywordVoid);
            keywords.Add("unsigned", TokenType.KeywordUnsigned);
            keywords.Add("typedef", TokenType.KeywordTypedef);
            keywords.Add("true", TokenType.KeywordTrue);
            keywords.Add("switch", TokenType.KeywordSwitch);
            keywords.Add("struct", TokenType.KeywordStruct);
            keywords.Add("string", TokenType.KeywordString);
            keywords.Add("static", TokenType.KeywordStatic);
            keywords.Add("sizeof", TokenType.KeywordSizeof);
            keywords.Add("signed", TokenType.KeywordSigned);
            keywords.Add("short", TokenType.KeywordShort);
            keywords.Add("register", TokenType.KeywordRegister);
            keywords.Add("return", TokenType.KeywordReturn);
            keywords.Add("printf", TokenType.KeywordPrintf);
            keywords.Add("long", TokenType.KeywordLong);
            keywords.Add("int", TokenType.KeywordInt);
            keywords.Add("#include", TokenType.KeywordInclude);
            keywords.Add("if", TokenType.KeywordIf);
            keywords.Add("goto", TokenType.KeywordGoto);
            keywords.Add("for", TokenType.KeywordFor);
            keywords.Add("float", TokenType.KeywordFloat);
            keywords.Add("false", TokenType.KeywordFalse);
            keywords.Add("extern", TokenType.KeywordExtern);
            keywords.Add("enum", TokenType.KeywordEnum);
            keywords.Add("else", TokenType.KeywordElse);
            keywords.Add("double", TokenType.KeywordDouble);
            keywords.Add("do", TokenType.KeywordDo);
            keywords.Add("default", TokenType.KeywordDefault);
            keywords.Add("date", TokenType.KeywordDate);
            keywords.Add("continue", TokenType.KeywordContinue);
            keywords.Add("const", TokenType.KeywordConst);
            keywords.Add("char", TokenType.KeywordChar);
            keywords.Add("case", TokenType.KeywordCase);
            keywords.Add("break", TokenType.KeywordBreak);
            keywords.Add("bool", TokenType.KeywordBool);
            keywords.Add("auto", TokenType.KeywordAuto);

            return keywords;
        }
    }
}
